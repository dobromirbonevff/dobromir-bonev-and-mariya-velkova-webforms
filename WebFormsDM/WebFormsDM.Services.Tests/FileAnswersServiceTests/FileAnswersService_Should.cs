﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebFormsDM.Data;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Services.Services;

namespace WebFormsDM.Services.Tests.FileAnswersServiceTests
{
    [TestClass]
    public class FileAnswersService_Should
    {
        [TestMethod]
        public async Task CreateFileAnswer_When_ParamsAreValid()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(CreateFileAnswer_When_ParamsAreValid));

            var form = new Form
            {
                Id = Guid.Parse("CC578963-5529-4DF4-1E54-08D80B2E57CD"),
                Title = "Most Favourit Colours"
            };

            var documentQuestion = new DocumentQuestion
            {
                Id = Guid.Parse("9133B6E8-D23E-40A5-9308-08D80B2F1C28"),
                Description = "What is your favorite colour",
                FormId = form.Id
            };

            using (var arrangeContext = new WebFormsDBContext(options))
            {
                arrangeContext.Forms.Add(form);
                arrangeContext.DocumentQuestions.Add(documentQuestion);
                arrangeContext.SaveChanges();
            }

            var documentQuestionAnswerDto = new DocumentQuestionAnswersDto
            {
                Id = Guid.NewGuid(),
                DocumentQuestionId = documentQuestion.Id,
                DocumentQuestion = documentQuestion,
                CorrelationToken = Guid.NewGuid(),
                AnsewrPaths = new List<string>() { "/Files/7be6e744-56cd-4d75-a07a-62b59d0d01cb_8d45eb8a8a5b9eaddeccb5705575c0be.jpg" }

            };

            var answerPath = documentQuestionAnswerDto.AnsewrPaths[0];
            var documentQAID = documentQuestionAnswerDto.Id;

            //Act & Assert
            using (var assertContext = new WebFormsDBContext(options))
            {
                var sut = new FileAnswersService(assertContext);
                var result = await sut.CreateFileAnswerAsync(answerPath, documentQAID);

                Assert.AreEqual(answerPath, result.FilePath);
                Assert.AreEqual(documentQAID, result.DocumentQAnswerId);
          

            }

        }
        [TestMethod]
        public async Task ThrowException_When_PathIsNull()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowException_When_PathIsNull));

            var form = new Form
            {
                Id = Guid.Parse("CC578963-5529-4DF4-1E54-08D80B2E57CD"),
                Title = "Most Favourit Colours"
            };

            var documentQuestion = new DocumentQuestion
            {
                Id = Guid.Parse("9133B6E8-D23E-40A5-9308-08D80B2F1C28"),
                Description = "What is your favorite colour",
                FormId = form.Id
            };

            using (var arrangeContext = new WebFormsDBContext(options))
            {
                arrangeContext.Forms.Add(form);
                arrangeContext.DocumentQuestions.Add(documentQuestion);
                arrangeContext.SaveChanges();
            }

            var documentQuestionAnswerDto = new DocumentQuestionAnswersDto
            {
                Id = Guid.NewGuid(),
                DocumentQuestionId = documentQuestion.Id,
                DocumentQuestion = documentQuestion,
                CorrelationToken = Guid.NewGuid(),
                AnsewrPaths = new List<string>() { "/Files/7be6e744-56cd-4d75-a07a-62b59d0d01cb_8d45eb8a8a5b9eaddeccb5705575c0be.jpg" }

            };

            var answerPath = documentQuestionAnswerDto.AnsewrPaths[0];
            var documentQAID = documentQuestionAnswerDto.Id;

            //Act & Assert
            using (var assertContext = new WebFormsDBContext(options))
            {
                var sut = new FileAnswersService(assertContext);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateFileAnswerAsync(null, documentQAID));
            }
        }

    }

}
