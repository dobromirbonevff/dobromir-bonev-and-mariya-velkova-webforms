﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebFormsDM.Data;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Services.Services;

namespace WebFormsDM.Services.Tests.TextQuestionAnswersServiceTests
{

    [TestClass]
    public class CreateTextQuestionAnswerAsync_Should
    {
        [TestMethod]
        public async Task CreateTextAnswer_When_ParamsAreValid()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(CreateTextAnswer_When_ParamsAreValid));

            var form = new Form
            {
                Id = Guid.Parse("154F5816-C208-415C-7160-08D80BA4E6CC"),
                Title = "Most Favourit Colours"
            };

            var textQuestion = new TextQuestion
            {
                Id = Guid.Parse("B0AD7043-4358-4DB6-2E40-08D80BA4E6CF"),
                Description = "What is your favorite colour",
                FormId = form.Id
            };

            using (var arrangeContext = new WebFormsDBContext(options))
            {
                arrangeContext.Forms.Add(form);
                arrangeContext.TextQuestions.Add(textQuestion);
                arrangeContext.SaveChanges();
            }

            var textQuestionAnswerDto = new TextQuestionAnswersDto
            {
                Id = Guid.NewGuid(),
                Answer = "blue",
                TextQuestionId = textQuestion.Id,
            };

            //Act & Assert
            using (var assertContext = new WebFormsDBContext(options))
            {
                var sut = new TextQuestionAnswersService(assertContext);
                var result =  await sut.CreateTextQuestionAnswerAsync(textQuestionAnswerDto);

                Assert.AreEqual(textQuestionAnswerDto.Answer, result.Answer);
                Assert.AreEqual(textQuestionAnswerDto.Id, result.Id);
                Assert.AreEqual(textQuestionAnswerDto.TextQuestionId, result.TextQuestionId);
                
            }
        }

        [TestMethod]
        public async Task ThrowException_When_DtoIsNull()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowException_When_DtoIsNull));

            //Act & Assert
            using (var assertContext = new WebFormsDBContext(options))
            {
                var sut = new TextQuestionAnswersService(assertContext);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateTextQuestionAnswerAsync(null));
            }
        }
    }
}

