﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WebFormsDM.Data;

namespace WebFormsDM.Services.Tests
{
 
    public static class Utilities
    {
        [TestMethod]
        public static DbContextOptions<WebFormsDBContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<WebFormsDBContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;

        }
    }
}
