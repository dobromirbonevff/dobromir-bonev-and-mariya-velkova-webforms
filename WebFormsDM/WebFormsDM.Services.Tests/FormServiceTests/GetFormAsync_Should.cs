﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebFormsDM.Data;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.Services;

namespace WebFormsDM.Services.Tests.FormServiceTests
{
    [TestClass]
    public class GetFormAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectForm_When_ParamsAreValid()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnCorrectForm_When_ParamsAreValid));
            var userId = Guid.NewGuid();

            var user = new User
            {
                Id = userId,
                UserName = "Test_User",
                Email = "Test_User@wm.com",
            };

            var hasher = new PasswordHasher<User>();
            user.PasswordHash = hasher.HashPassword(user, "user");

            var form = new Form
            {
                Description = "Test Description",
                Title = "Test Title",
                UserId = userId
            };

            using (var arrangeContext = new WebFormsDBContext(options))
            {
                arrangeContext.Forms.Add(form);
                arrangeContext.Users.Add(user);
                arrangeContext.SaveChanges();
            }

            //Act & Assert
            using (var assertContext = new WebFormsDBContext(options))
            {
                var sut = new FormService(assertContext);
                var formResult = await assertContext.Forms.FirstOrDefaultAsync();
                var result = await sut.GetFormAsync(formResult.Id);

                Assert.AreEqual(form.Description, result.Description);
                Assert.AreEqual(form.Title, result.Title);
                Assert.AreEqual(form.UserId, result.UserId);
            }
        }

        [TestMethod]
        public async Task ThrowException_When_FormNotFound()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowException_When_FormNotFound));
            var guid = Guid.NewGuid();

            //Act & Assert
            using (var assertContext = new WebFormsDBContext(options))
            {
                var sut = new FormService(assertContext);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetFormAsync(guid));
            }
        }
    }
}
