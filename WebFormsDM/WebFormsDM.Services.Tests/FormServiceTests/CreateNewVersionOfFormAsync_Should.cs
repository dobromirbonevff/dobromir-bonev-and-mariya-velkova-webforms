﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebFormsDM.Data;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Services.Services;

namespace WebFormsDM.Services.Tests.FormServiceTests
{
    [TestClass]
    public class CreateNewVersionOfFormAsync_Should
    {
        [TestMethod]
        public async Task CreateNewVersionOfForm_When_ParamsAreValid()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(CreateNewVersionOfForm_When_ParamsAreValid));
            var userId = Guid.NewGuid();

            var user = new User
            {
                Id = userId,
                UserName = "Test_User",
                Email = "Test_User@wm.com",
            };

            var hasher = new PasswordHasher<User>();
            user.PasswordHash = hasher.HashPassword(user, "user");

            var form = new Form
            {
                Description = "Test Description",
                Title = "Test Title(New Version)",
                UserId = userId
            };

            var formDto = new FormDto
            {
                Description = "Test Description",
                Title = "Test Title",
                UserId = userId,
                TextQuestions = new List<TextQuestionDto>(),
                OptionQuestions = new List<OptionQuestionsDto>(),
                DocumentQuestions = new List<DocumentQuestionDto>()
            };

            using (var arrangeContext = new WebFormsDBContext(options))
            {
                arrangeContext.Forms.Add(form);
                arrangeContext.Users.Add(user);
                arrangeContext.SaveChanges();
            }

            //Act & Assert
            using (var assertContext = new WebFormsDBContext(options))
            {
                var sut = new FormService(assertContext);
                var result = await sut.CreateNewVersionOfFormAsync(formDto);

                Assert.AreEqual(form.Description, result.Description);
                Assert.AreEqual(form.Title, result.Title);
                Assert.AreEqual(form.UserId, result.UserId);
            }
        }

        [TestMethod]
        public async Task ThrowWhen_NewFormIsNull()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowWhen_NewFormIsNull));

            //Act & Assert
            using (var assertContext = new WebFormsDBContext(options))
            {
                var sut = new FormService(assertContext);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateNewVersionOfFormAsync(null));
            }
        }
    }
}
