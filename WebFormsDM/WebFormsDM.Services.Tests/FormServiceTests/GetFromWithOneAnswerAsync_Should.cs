﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebFormsDM.Data;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.Services;

namespace WebFormsDM.Services.Tests.FormServiceTests
{
    [TestClass]
    public class GetFromWithOneAnswerAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectFormWithOneAnswer_When_ParamsAreValid()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnCorrectFormWithOneAnswer_When_ParamsAreValid));
            var userId = Guid.NewGuid();
            var correlationToken = Guid.NewGuid();

            var user = new User
            {
                Id = userId,
                UserName = "Test_User",
                Email = "Test_User@wm.com",
            };

            var hasher = new PasswordHasher<User>();
            user.PasswordHash = hasher.HashPassword(user, "user");

            var textQuestionAnswer = new TextQuestionAnswers()
            {
                Answer = "Text Answer",
                CorrelationToken = correlationToken
            };

            var textQuestion = new TextQuestion()
            {
                Description = "Test Text Question",
                Answers = new List<TextQuestionAnswers>()
            };
            textQuestion.Answers.Add(textQuestionAnswer);



            var form = new Form
            {
                Description = "Test Description",
                Title = "Test Title",
                UserId = userId,
                TextQuestions = new List<TextQuestion>()
            };
            form.TextQuestions.Add(textQuestion);

            using (var arrangeContext = new WebFormsDBContext(options))
            {
                arrangeContext.Forms.Add(form);
                arrangeContext.Users.Add(user);
                arrangeContext.SaveChanges();
            }

            //Act & Assert
            using (var assertContext = new WebFormsDBContext(options))
            {
                var sut = new FormService(assertContext);
                var formResult = await assertContext.Forms.FirstOrDefaultAsync();
                var result = await sut.GetFromWithOneAnswerAsync(formResult.Id, correlationToken);

                Assert.AreEqual(form.Description, result.Description);
                Assert.AreEqual(form.Title, result.Title);
                Assert.AreEqual(form.UserId, result.UserId);
            }
        }

        [TestMethod]
        public async Task ReturnCorrectAnswersCount_FormWithOneAnswer_When_ParamsAreValid()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnCorrectAnswersCount_FormWithOneAnswer_When_ParamsAreValid));
            var userId = Guid.NewGuid();
            var correlationToken = Guid.NewGuid();

            var user = new User
            {
                Id = userId,
                UserName = "Test_User",
                Email = "Test_User@wm.com",
            };

            var hasher = new PasswordHasher<User>();
            user.PasswordHash = hasher.HashPassword(user, "user");

            var textQuestionAnswer = new TextQuestionAnswers()
            {
                Answer = "Text Answer",
                CorrelationToken = correlationToken
            };

            var textQuestion = new TextQuestion()
            {
                Description = "Test Text Question",
                Answers = new List<TextQuestionAnswers>()
            };
            textQuestion.Answers.Add(textQuestionAnswer);



            var form = new Form
            {
                Description = "Test Description",
                Title = "Test Title",
                UserId = userId,
                TextQuestions = new List<TextQuestion>()
            };
            form.TextQuestions.Add(textQuestion);

            using (var arrangeContext = new WebFormsDBContext(options))
            {
                arrangeContext.Forms.Add(form);
                arrangeContext.Users.Add(user);
                arrangeContext.SaveChanges();
            }

            //Act & Assert
            using (var assertContext = new WebFormsDBContext(options))
            {
                var sut = new FormService(assertContext);
                var formResult = await assertContext.Forms.FirstOrDefaultAsync();
                var result = await sut.GetFromWithOneAnswerAsync(formResult.Id, correlationToken);
                var formTextQuestion = result.TextQuestions.FirstOrDefault();

                Assert.AreEqual(1, formTextQuestion.Answers.Count());
            }
        }
    }
}
