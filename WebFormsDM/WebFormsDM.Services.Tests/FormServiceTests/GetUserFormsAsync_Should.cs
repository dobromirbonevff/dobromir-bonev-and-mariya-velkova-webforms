﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebFormsDM.Data;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.Services;

namespace WebFormsDM.Services.Tests.FormServiceTests
{
    [TestClass]
    public class GetUserFormsAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectForm_When_UserIdIsValid()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnCorrectForm_When_UserIdIsValid));
            var userId = Guid.NewGuid();

            var user = new User
            {
                Id = userId,
                UserName = "Test_User",
                Email = "Test_User@wm.com",
            };

            var hasher = new PasswordHasher<User>();
            user.PasswordHash = hasher.HashPassword(user, "user");

            var form = new Form
            {
                Description = "Test Description",
                Title = "Test Title",
                UserId = userId
            };

            using (var arrangeContext = new WebFormsDBContext(options))
            {
                arrangeContext.Users.Add(user);
                arrangeContext.Forms.Add(form);
                arrangeContext.SaveChanges();
            }

            //Act & Assert
            using (var assertContext = new WebFormsDBContext(options))
            {
                var sut = new FormService(assertContext);
                var result = await sut.GetUserFormsAsync(userId);

                Assert.AreEqual(1, result.Count);
                Assert.AreEqual(form.Description, result.ToList()[0].Description);
                Assert.AreEqual(form.Title, result.ToList()[0].Title);
                Assert.AreEqual(form.UserId, result.ToList()[0].UserId);
            }
        }

        [TestMethod]
        public async Task ThrowException_When_UsersIdIsNull()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowException_When_UsersIdIsNull));

            //Act & Assert
            using (var assertContext = new WebFormsDBContext(options))
            {
                var sut = new FormService(assertContext);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetUserFormsAsync(null));
            }
        }
    }
}
