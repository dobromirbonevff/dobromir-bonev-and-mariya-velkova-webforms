﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebFormsDM.Data;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.Contracts;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Services.Services;

namespace WebFormsDM.Services.Tests.FillFormServiceTests
{
    [TestClass]
    public class GetFillFormAsync_Should
    {
        [TestMethod]
        public async Task GetFillForm_When_ParamsAreValid()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(GetFillForm_When_ParamsAreValid));

            var fileAnswerServiceMock = new Mock<IFileAnswersService>();
            var documentQuestionAnswersServiceMock = new Mock<IDocumentQuestionAnswersService>();
            var textQuestionAnswersServiceMock = new Mock<ITextQuestionAnswersService>();
            var optionQuestionsAnswersServiceMock = new Mock<IOptionQuestionsAnswersService>();

            var form = new Form
            {
                Id = Guid.NewGuid(),
                Description = "Test Description",
                Title = "Test Title",
                TextQuestions = new List<TextQuestion>(),
                OptionsQuestions = new List<OptionQuestions>(),
                DocumentQuestions = new List<DocumentQuestion>()
            };
    

            using (var arrangeContext = new WebFormsDBContext(options))
            {
                arrangeContext.Forms.Add(form);
                arrangeContext.SaveChanges();
            }

            var fillFormDto = new FillFormDto
            {
                Id = form.Id,
                Description = form.Description,
                Title = form.Title,
                TextQuestionAndAnswerDtos = new List<TextQuestionAndAnswersDto>(),
                OptionQuestionsAndAnswersDtos = new List<OptionQuestionsAndAnswersDto>(),
                DocumentQuestionAndAnswerDto = new List<DocumentQuestionAndAnswersDto>()
            };


            //Act & Assert
            using (var assertContext = new WebFormsDBContext(options))
            {
                var sut = new FillFormService(assertContext,
                    fileAnswerServiceMock.Object,
                    documentQuestionAnswersServiceMock.Object,
                    textQuestionAnswersServiceMock.Object,
                    optionQuestionsAnswersServiceMock.Object);

                var result = await sut.GetFillFormAsync(form.Id);

                Assert.AreEqual(fillFormDto.Id, result.Id);
                Assert.AreEqual(fillFormDto.Title, result.Title);
                Assert.AreEqual(fillFormDto.Description, result.Description);


            }

        }

        [TestMethod]
        public async Task ThrowException_When_FormNotFound()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowException_When_FormNotFound));

            var fileAnswerServiceMock = new Mock<IFileAnswersService>();
            var documentQuestionAnswersServiceMock = new Mock<IDocumentQuestionAnswersService>();
            var textQuestionAnswersServiceMock = new Mock<ITextQuestionAnswersService>();
            var optionQuestionsAnswersServiceMock = new Mock<IOptionQuestionsAnswersService>();

            var form = new Form
            {
                Id = Guid.NewGuid(),
                Description = "Test Description",
                Title = "Test Title",
                TextQuestions = new List<TextQuestion>(),
                OptionsQuestions = new List<OptionQuestions>(),
                DocumentQuestions = new List<DocumentQuestion>()
            };


            using (var arrangeContext = new WebFormsDBContext(options))
            {
                arrangeContext.Forms.Add(form);
                arrangeContext.SaveChanges();
            }
            Guid testGuid = Guid.NewGuid();

            //Act & Assert
            using (var assertContext = new WebFormsDBContext(options))
            {
                var sut = new FillFormService(assertContext,
                   fileAnswerServiceMock.Object,
                   documentQuestionAnswersServiceMock.Object,
                   textQuestionAnswersServiceMock.Object,
                   optionQuestionsAnswersServiceMock.Object);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetFillFormAsync(testGuid));
            }
        }
    }

}
