﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebFormsDM.Data;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Services.Services;

namespace WebFormsDM.Services.Tests.DocumentQuestionAnswersServiceTests
{
    [TestClass]
    public class CreateDocumentQuestionAnswerAsync_Should
    {
        [TestMethod]

        public async Task CreateDocumentQuestionAnswer_When_ParamsAreValid()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(CreateDocumentQuestionAnswer_When_ParamsAreValid));

            var form = new Form
            {
                Id = Guid.Parse("CC578963-5529-4DF4-1E54-08D80B2E57CD"),
                Title = "Most Favourit Colours"
            };

            var documentQuestion = new DocumentQuestion
            {
                Id = Guid.Parse("9133B6E8-D23E-40A5-9308-08D80B2F1C28"),
                Description = "What is your favorite colour",
                FormId = form.Id
            };
            using (var arrangeContext = new WebFormsDBContext(options))
            {
                arrangeContext.Forms.Add(form);
                arrangeContext.DocumentQuestions.Add(documentQuestion);
                arrangeContext.SaveChanges();
            }

            var documentQuestionAnswerDto = new DocumentQuestionAnswersDto
            {
                Id = Guid.NewGuid(),
                DocumentQuestionId = documentQuestion.Id,
                CorrelationToken = Guid.NewGuid()

            };

            //Act & Assert
            using (var assertContext = new WebFormsDBContext(options))
            {
                var sut = new DocumentQuestionAnswersService(assertContext);
                var result = await sut.CreateDocumentQuestionAnswerAsync(documentQuestionAnswerDto);


                Assert.AreEqual(documentQuestionAnswerDto.Id, result.Id);
                Assert.AreEqual(documentQuestionAnswerDto.DocumentQuestionId, result.DocumentQuestionId);
                Assert.AreEqual(documentQuestionAnswerDto.CorrelationToken, result.CorrelationToken);

            }

        }


        [TestMethod]
        public async Task ThrowException_When_DtoIsNull()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowException_When_DtoIsNull));

            //Act & Assert
            using (var assertContext = new WebFormsDBContext(options))
            {
                var sut = new DocumentQuestionAnswersService(assertContext);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateDocumentQuestionAnswerAsync(null));
            }
        }
    }
}

