﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebFormsDM.Data;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Services.Services;

namespace WebFormsDM.Services.Tests.OptionQuestionsAnswersServiceTests
{
    [TestClass]
    public class OptionQuestionAnswersService_Should
    {
        [TestMethod]
        public async Task CreateOptionQuestionAnswer_When_ParamsAreValid()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(CreateOptionQuestionAnswer_When_ParamsAreValid));

            var form = new Form
            {
                Id = Guid.Parse("CC578963-5529-4DF4-1E54-08D80B2E57CD"),
                Title = "Most Favourit Colours"
            };

            var optionQuestion = new OptionQuestions
            {
                Id = Guid.Parse("9133B6E8-D23E-40A5-9308-08D80B2F1C28"),
                Description = "What is your favorite colour",
                FormId = form.Id
            };
          
            using (var arrangeContext = new WebFormsDBContext(options))
            {
                arrangeContext.Forms.Add(form);
                arrangeContext.OptionsQuestions.Add(optionQuestion);
                arrangeContext.SaveChanges();
            }

            var optionQuestionAnswerDto = new OptionQuestionsAnswersDto
            {
                Id = Guid.NewGuid(),
                OptionsQuestionId = optionQuestion.Id,
                CorrelationToken = Guid.NewGuid(),
                Answer = "blue"

            };

            //Act & Assert
            using (var assertContext = new WebFormsDBContext(options))
            {
                var sut = new OptionQuestionsAnswersService(assertContext);
                var result = await sut.CreateOptionQuestionAnswerAsync(optionQuestionAnswerDto);

                Assert.AreEqual(optionQuestionAnswerDto.Id, result.Id);
                Assert.AreEqual(optionQuestionAnswerDto.OptionsQuestionId, result.OptionsQuestionId);
                Assert.AreEqual(optionQuestionAnswerDto.CorrelationToken, result.CorrelationToken);
                Assert.AreEqual(optionQuestionAnswerDto.Answer, result.Answer);
            }
        }

        [TestMethod]
        public async Task ThrowException_When_DtoIsNull()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowException_When_DtoIsNull));

            //Act & Assert
            using (var assertContext = new WebFormsDBContext(options))
            {
                var sut = new OptionQuestionsAnswersService(assertContext);

                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.CreateOptionQuestionAnswerAsync(null));
            }
        }
    }
}
