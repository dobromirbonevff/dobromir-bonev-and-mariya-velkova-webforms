﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Web.Models;

namespace WebFormsDM.Web.ViewModelsMapper
{
    public static class OptionQuestionsAnswersViewModelExtension
    {
        public static OptionQuestionsAnswersDto GetDto(this OptionQuestionsAnswersViewModel optionQuestionsAnswersViewModel)
        {
            var optionQuestionsAnswersDto = new OptionQuestionsAnswersDto
            {
                Id = optionQuestionsAnswersViewModel.Id,
                //OptionsQuestionDescription = optionQuestionsAnswers.OptionsQuestion.Description,
                OptionsQuestionId = optionQuestionsAnswersViewModel.OptionsQuestionId,
                CorrelationToken = optionQuestionsAnswersViewModel.CorrelationToken,
                //Options = optionQuestionsAnswersViewModel.Options.GetDtos(),
                Answer = optionQuestionsAnswersViewModel.Answer

            };

            return optionQuestionsAnswersDto;
        }


        public static ICollection<OptionQuestionsAnswersDto> GetDtos
            (this ICollection<OptionQuestionsAnswersViewModel> optionQuestionsAnswers)
        {
            return optionQuestionsAnswers.Select(GetDto).ToList();
        }

        public static OptionQuestionsAnswersViewModel GetViewModel(this OptionQuestionsAnswersDto optionQuestionsAnswersDto)
        {
            var optionQuestionsAnswersVM = new OptionQuestionsAnswersViewModel
            {
                Id = optionQuestionsAnswersDto.Id,
                //OptionsQuestionDescription = optionQuestionsAnswers.OptionsQuestion.Description,
                OptionsQuestionId = optionQuestionsAnswersDto.OptionsQuestionId,
                CorrelationToken = optionQuestionsAnswersDto.CorrelationToken,
                //Options = optionQuestionsAnswersDto.Options.GetViewModels(),
                Answer = optionQuestionsAnswersDto.Answer
            };

            return optionQuestionsAnswersVM;
        }

        public static List<OptionQuestionsAnswersViewModel> GetEntities
            (this ICollection<OptionQuestionsAnswersDto> optionQuestionsAnswersDtos)
        {
            return optionQuestionsAnswersDtos.Select(GetViewModel).ToList();
        }
    }
}
