﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Web.Models;

namespace WebFormsDM.Web.ViewModelsMapper
{
    public static class DocumentQuestionViewModelExtension
    {
        public static DocumentQuestionDto GetDto(this DocumentQuestionViewModel documentQuestionViewModel)
        {
            var documnetQuestionFileSize = int.Parse(documentQuestionViewModel.MaxFileSize
                .Substring(0, documentQuestionViewModel.MaxFileSize.Length - 2));

            var documentQuestionDto = new DocumentQuestionDto()
            {
                Id = documentQuestionViewModel.Id,
                Description = documentQuestionViewModel.Description,
                IsRequired = documentQuestionViewModel.IsRequired,
                FormId = documentQuestionViewModel.FormId,
                OrderNumber = documentQuestionViewModel.OrderNumber,
                IsDeleted = documentQuestionViewModel.IsDeleted,
                NumberOfFiles = documentQuestionViewModel.NumberOfFiles,
                FileSize = documnetQuestionFileSize,
                
            };

            return documentQuestionDto;
        }

        public static ICollection<DocumentQuestionDto> GetDtos(this List<DocumentQuestionViewModel> documentQuestionViewModels)
        {
            var documentQuestionDtos = documentQuestionViewModels.Select(GetDto).ToList();

            return documentQuestionDtos;
        }

        //FROM DTOS TO VIEW MODELS
        public static DocumentQuestionViewModel GetViewModel(this DocumentQuestionDto documentQuestionDto)
        {
            if (documentQuestionDto == null)
            {
                throw new ArgumentNullException("DocumentQuestionDto cannot be null");
            }

            return new DocumentQuestionViewModel
            {
                Id = documentQuestionDto.Id,
                Description = documentQuestionDto.Description,
                IsDeleted = documentQuestionDto.IsDeleted,
                FormId = documentQuestionDto.FormId,
                OrderNumber = documentQuestionDto.OrderNumber,
                IsRequired = documentQuestionDto.IsRequired,
                NumberOfFiles = documentQuestionDto.NumberOfFiles,
                MaxFileSize = documentQuestionDto.FileSize.ToString() + "MB",
                Answers = documentQuestionDto.Answers.GetViewModels()
            };
        }

        public static List<DocumentQuestionViewModel> GetViewModels(this ICollection<DocumentQuestionDto> documentQuestionDtos)
        {
            var documentQuestionViewModels = documentQuestionDtos.Select(GetViewModel).ToList();

            return documentQuestionViewModels;
        }
    }
}
