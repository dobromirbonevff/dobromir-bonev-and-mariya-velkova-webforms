﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Web.Models;

namespace WebFormsDM.Web.ViewModelsMapper
{
    public static class TextQuestionViewModelExtension
    {      
        public static TextQuestionDto GetDto(this TextQuestionViewModel textQuestionViewModel)
        {
            var textQuestionDto = new TextQuestionDto()
            {
                Id = textQuestionViewModel.Id,
                Description = textQuestionViewModel.Description,
                IsLongAnswer = textQuestionViewModel.IsLongAnswer,
                IsRequired = textQuestionViewModel.IsRequired,
                OrderNumber = textQuestionViewModel.OrderNumber,
                FormId = textQuestionViewModel.FormId,
                FormTitle = textQuestionViewModel.FormTitle,
                Answers = textQuestionViewModel.Answers.GetDtos()

            };

            return textQuestionDto;
        }

        public static ICollection<TextQuestionDto> GetDtos(this List<TextQuestionViewModel> textQuestionViewModels)
        {
            var textQuestionDtos = textQuestionViewModels.Select(GetDto).ToList();

            return textQuestionDtos;
        }

        //FROM DTOS TO VIEW MODELS
        public static TextQuestionViewModel GetViewModel(this TextQuestionDto textQuestionDto)
        {
            if (textQuestionDto == null)
            {
                throw new ArgumentNullException("TextQuestionDto cannot be null");
            }

            return new TextQuestionViewModel
            {
                Id = textQuestionDto.Id,
                Description = textQuestionDto.Description,
                IsLongAnswer = textQuestionDto.IsLongAnswer,
                IsRequired = textQuestionDto.IsRequired,
                OrderNumber = textQuestionDto.OrderNumber,
                FormId = textQuestionDto.FormId,
                FormTitle = textQuestionDto.FormTitle,
                Answers = textQuestionDto.Answers.GetViewModels()
            };
        }
        public static List<TextQuestionViewModel> GetViewModels(this ICollection<TextQuestionDto> textQuestionDtos)
        {
            var textQuestionViewModels = textQuestionDtos.Select(GetViewModel).ToList();

            return textQuestionViewModels;
        }
    }

}
