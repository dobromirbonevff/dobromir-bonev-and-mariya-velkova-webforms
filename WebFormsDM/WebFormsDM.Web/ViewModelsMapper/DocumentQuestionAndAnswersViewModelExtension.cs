﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Web.Models;

namespace WebFormsDM.Web.ViewModelsMapper
{
    public static class DocumentQuestionAndAnswersViewModelExtension
    {
        public static DocumentQuestionAndAnswersViewModel GetDocumentQuestionAndAnswerVM(this DocumentQuestionAndAnswersDto documentQuestionAndAnswerDto)
        {
            var documentQuestionAndAnswerViewModel = new DocumentQuestionAndAnswersViewModel
            {
                DocumentQuestionAnswersViewModel = new DocumentQuestionAnswersViewModel(),
                DocumentQuestionViewModel = documentQuestionAndAnswerDto.DocumentQuestionDto.GetViewModel()
            };

            return documentQuestionAndAnswerViewModel;
        }

        public static List<DocumentQuestionAndAnswersViewModel> GetDocumentQuestionAndAnswersVM(this ICollection<DocumentQuestionAndAnswersDto> documentQuestionAndAnswersDtos)
        {
            var documentQuestionAndAnswersVM = documentQuestionAndAnswersDtos.Select(GetDocumentQuestionAndAnswerVM).ToList();

            return documentQuestionAndAnswersVM;
        }
        public static DocumentQuestionAndAnswersDto GetDocumentQuestionAndAnswerDto(this DocumentQuestionAndAnswersViewModel documentQuestionAndAnswerVM)
        {
            var documentQuestionAndAnswerDto = new DocumentQuestionAndAnswersDto
            {
                DocumentQuestionDto = documentQuestionAndAnswerVM.DocumentQuestionViewModel.GetDto(),
                DocumentQuestionAnswersDto = documentQuestionAndAnswerVM.DocumentQuestionAnswersViewModel.GetDto()
            };

            return documentQuestionAndAnswerDto;
        }


        public static List<DocumentQuestionAndAnswersDto> GetDocumentQuestionAndAnswersDtos(this ICollection<DocumentQuestionAndAnswersViewModel> documentQuestionAndAnswersVMs)
        {
            var documentQuestionAndAnswersDtos = documentQuestionAndAnswersVMs.Select(GetDocumentQuestionAndAnswerDto).ToList();

            return documentQuestionAndAnswersDtos;
        }
    }
}
