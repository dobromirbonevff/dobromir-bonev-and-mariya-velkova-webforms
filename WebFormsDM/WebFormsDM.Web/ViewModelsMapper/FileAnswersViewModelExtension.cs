﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Web.Models;

namespace WebFormsDM.Web.ViewModelsMapper
{
    public static class FileAnswersViewModelExtension
    {
        public static FileAnswersDto GetDto(this FileAnswersViewModel fileAnswersViewModel)
        {
            var fileAnswersDto = new FileAnswersDto()
            {
                Id = fileAnswersViewModel.Id,
                FilePath = fileAnswersViewModel.FilePath,
                DocumentQAnswerId = fileAnswersViewModel.DocumentQAnswerId,
               // DocumentQuestionAswerDto = fileAnswersViewModel.DocumentQuestionAswer


            };

            return fileAnswersDto;
        }



        public static ICollection<FileAnswersDto> GetDtos(this ICollection<FileAnswersViewModel> fileAnswersViewModels)
        {
            var fileAnswersDtos = fileAnswersViewModels.Select(GetDto).ToList();

            return fileAnswersDtos;
        }

        //FROM DTOS TO VIEW MODELS
        public static FileAnswersViewModel GetViewModel(this FileAnswersDto fileAnswersDto)
        {
            if (fileAnswersDto == null)
            {
                throw new ArgumentNullException("FileAnswersDto cannot be null");
            }

            return new FileAnswersViewModel
            {
                Id = fileAnswersDto.Id,
                FilePath = fileAnswersDto.FilePath,
                DocumentQAnswerId = fileAnswersDto.DocumentQAnswerId,

            };
        }

        public static ICollection<FileAnswersViewModel> GetViewModels(this ICollection<FileAnswersDto> fileAnswersDto)
        {
            var textQuestionAnswersViewModels = fileAnswersDto.Select(GetViewModel).ToList();

            return textQuestionAnswersViewModels;
        }
    }
}
