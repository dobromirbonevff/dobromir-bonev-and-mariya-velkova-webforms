﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Web.Models;

namespace WebFormsDM.Web.ViewModelsMapper
{
    public static class FillFormViewModelExtension
    {
        //FROM VIEW MODELS TO DTOS

        public static FillFormDto GetDto(this FillFormViewModel fillFormViewModel)
        {
            var fillFormDto = new FillFormDto()
            {
                Id = fillFormViewModel.Id,
                Title = fillFormViewModel.Title,
                Description = fillFormViewModel.Description,
                TextQuestionAndAnswerDtos = fillFormViewModel.TextQuestionAndAnswersViewModels.GetTQAVDtos(),
                DocumentQuestionAndAnswerDto = fillFormViewModel.DocumentQuestionsAndAnswers.GetDocumentQuestionAndAnswersDtos(),
                OptionQuestionsAndAnswersDtos = fillFormViewModel.OptionQuestionsAndAnswersViewModels.GetOptionQuestionAndAnswersDto(),

            };

            return fillFormDto;
        }

        public static List<FillFormDto> GetDtos(this List<FillFormViewModel> fillFormViewModels)
        {
            var fillFormDtos = fillFormViewModels.Select(GetDto).ToList();

            return fillFormDtos;
        }

        //FROM DTOS TO VIEW MODELS
        public static FillFormViewModel GetViewModel(this FillFormDto fillFormDto)
        {
            if (fillFormDto == null)
            {
                throw new ArgumentNullException("FillFormDto cannot be null");
            }
          
            return new FillFormViewModel
            {
                Id = fillFormDto.Id,
                Title = fillFormDto.Title,
                Description = fillFormDto.Description,
                TextQuestionAndAnswersViewModels = fillFormDto.TextQuestionAndAnswerDtos.GetTQAVMs(),
                DocumentQuestionsAndAnswers = fillFormDto.DocumentQuestionAndAnswerDto.GetDocumentQuestionAndAnswersVM(),
                OptionQuestionsAndAnswersViewModels = fillFormDto.OptionQuestionsAndAnswersDtos.GetOptionQuestionAndAnswersVM(),
                AllQuestionsCount = fillFormDto.AllQuestionsCount
            };
        }

        public static ICollection<FillFormViewModel> GetViewModels(this ICollection<FillFormDto> fillFormDtos)
        {
            var fillFormViewModels = fillFormDtos.Select(GetViewModel).ToList();

            return fillFormViewModels;
        }


    }
}



