﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Web.Models;

namespace WebFormsDM.Web.ViewModelsMapper
{
    public static class OptionsQuestionOptionViewModelExtension
    {
        //FROM VIEW MODELS TO DTOS

        public static OptionsQuestionOptionDto GetDto(this OptionsQuestionOptionViewModel optionsQuestionOptionViewModel)
        {
            var optionDto = new OptionsQuestionOptionDto
            {
                Id = optionsQuestionOptionViewModel.Id,
                Option = optionsQuestionOptionViewModel.Option,
                OptionsQuestionId = optionsQuestionOptionViewModel.OptionsQuestionId,
               // Answer = optionsQuestionOptionViewModel.Answer.GetDto()

            };

            return optionDto;
        }

        public static ICollection<OptionsQuestionOptionDto> GetDtos(
            this List<OptionsQuestionOptionViewModel> optionsQuestionOptionViewModels)
        {
            return optionsQuestionOptionViewModels.Select(GetDto).ToList();
        }

        //FROM DTOS TO VIEW MODELS

        public static OptionsQuestionOptionViewModel GetViewModel(this OptionsQuestionOptionDto optionsQuestionOptionDto)
        {
            var optionViewModel = new OptionsQuestionOptionViewModel
            {
                Id = optionsQuestionOptionDto.Id,
                Option = optionsQuestionOptionDto.Option,
                OptionsQuestionId = optionsQuestionOptionDto.OptionsQuestionId,
                //Answer = optionsQuestionOptionDto.Answer.GetViewModel()
            };

            return optionViewModel;
        }

        public static List<OptionsQuestionOptionViewModel> GetViewModels(
            this ICollection<OptionsQuestionOptionDto> optionsQuestionOptionDtos)
        {
            return optionsQuestionOptionDtos.Select(GetViewModel).ToList();
        }
    }
}
