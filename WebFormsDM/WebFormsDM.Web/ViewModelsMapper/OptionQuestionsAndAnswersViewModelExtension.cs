﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Web.Models;

namespace WebFormsDM.Web.ViewModelsMapper
{
    public static class OptionQuestionsAndAnswersViewModelExtension
    {
        public static OptionQuestionsAndAnswersViewModel GetOptionQuestionAndAnswerVM
            (this OptionQuestionsAndAnswersDto optionQuestionsAndAnswersDto)
        {
            var optionQuestionsAndAnswersViewModel = new OptionQuestionsAndAnswersViewModel
            {
                 OptionQuestionsAnswersViewModel = new OptionQuestionsAnswersViewModel(),
                 OptionQuestionsViewModel = optionQuestionsAndAnswersDto.OptionQuestionsDto.GetViewModel()
            };

            return optionQuestionsAndAnswersViewModel;
        }

        public static List<OptionQuestionsAndAnswersViewModel> GetOptionQuestionAndAnswersVM
            (this ICollection<OptionQuestionsAndAnswersDto> optionQuestionsAndAnswers)
        {
            return optionQuestionsAndAnswers.Select(GetOptionQuestionAndAnswerVM).ToList();
        }

        //From VM to Dto
        public static OptionQuestionsAndAnswersDto GetOptionQuestionAndAnswerDto
      (this OptionQuestionsAndAnswersViewModel optionQuestionsAndAnswersVM)
        {
            var optionQuestionsAndAnswersDto = new OptionQuestionsAndAnswersDto
            {
                OptionQuestionsAnswersDto = optionQuestionsAndAnswersVM.OptionQuestionsAnswersViewModel.GetDto(),
                OptionQuestionsDto = optionQuestionsAndAnswersVM.OptionQuestionsViewModel.GetDto()
            };

            return optionQuestionsAndAnswersDto;
        }

        public static List<OptionQuestionsAndAnswersDto> GetOptionQuestionAndAnswersDto
            (this ICollection<OptionQuestionsAndAnswersViewModel> optionQuestionsAndAnswersVM)
        {
            return optionQuestionsAndAnswersVM.Select(GetOptionQuestionAndAnswerDto).ToList();
        }

    }
}
