﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Web.Models;

namespace WebFormsDM.Web.ViewModelsMapper
{
    public static class TextQuestionAndAnswersViewModelExtension
    {
        public static TextQuestionAndAnswersViewModel GetTextQuestionAndAnswerVM(this TextQuestionAndAnswersDto questionAndAnswerDto)
        {
            var textQuestionAndAnswerViewModel = new TextQuestionAndAnswersViewModel
            {
                TextQuestionAnswersViewModel = new TextQuestionAnswersViewModel(),
                TextQuestionViewModel = questionAndAnswerDto.TextQuestionDto.GetViewModel()
                
            };

            return textQuestionAndAnswerViewModel;
        }

        public static List<TextQuestionAndAnswersViewModel> GetTQAVMs(this ICollection<TextQuestionAndAnswersDto> dtos)
        {
            return dtos.Select(GetTextQuestionAndAnswerVM).ToList();
        }

        public static TextQuestionAndAnswersDto GetTextQuestionAndAnswerDto(this TextQuestionAndAnswersViewModel questionAndAnswerVM)
        {
            var textQuestionAndAnswerDto = new TextQuestionAndAnswersDto
            {
                TextQuestionAnswersDto = questionAndAnswerVM.TextQuestionAnswersViewModel.GetDto(),
                TextQuestionDto = questionAndAnswerVM.TextQuestionViewModel.GetDto()

            };

            return textQuestionAndAnswerDto;
        }

        public static List<TextQuestionAndAnswersDto> GetTQAVDtos(this ICollection<TextQuestionAndAnswersViewModel> models)
        {
            return models.Select(GetTextQuestionAndAnswerDto).ToList();
        }


    }
}
