﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Web.Models;

namespace WebFormsDM.Web.ViewModelsMapper
{
    public static class TextQuestionAnswersViewModelExtension
    {
        public static TextQuestionAnswersDto GetDto(this TextQuestionAnswersViewModel textQuestionAnswersViewModel)
        {
            var textQuestionAnswersDto = new TextQuestionAnswersDto()
            {
                Id = textQuestionAnswersViewModel.Id,
                Answer = textQuestionAnswersViewModel.Answer,
                TextQuestionId = textQuestionAnswersViewModel.TextQuestionId,
               // TextQuestion = textQuestionAnswersViewModel.TextQuestion.GetDto(),
                CorrelationToken = textQuestionAnswersViewModel.CorrelationToken

            };

            return textQuestionAnswersDto;
        }


        public static ICollection<TextQuestionAnswersDto> GetDtos(this ICollection<TextQuestionAnswersViewModel> textQuestionAnswersViewModels)
        {
            var documentQuestionAnswersDtos = textQuestionAnswersViewModels.Select(GetDto).ToList();

            return documentQuestionAnswersDtos;
        }

        //FROM DTOS TO VIEW MODELS
        public static TextQuestionAnswersViewModel GetViewModel(this TextQuestionAnswersDto textQuestionAnswersDto)
        {
            if (textQuestionAnswersDto == null)
            {
                throw new ArgumentNullException("TextQuestionAnswerDto cannot be null");
            }

            return new TextQuestionAnswersViewModel
            {
                Id = textQuestionAnswersDto.Id,
                Answer = textQuestionAnswersDto.Answer,
                TextQuestionId = textQuestionAnswersDto.TextQuestionId,
               // TextQuestion = textQuestionAnswersDto.TextQuestion.GetViewModel(),
                CorrelationToken = textQuestionAnswersDto.CorrelationToken

            };
        }

        public static ICollection<TextQuestionAnswersViewModel> GetViewModels(this ICollection<TextQuestionAnswersDto> textQuestionAnswersDto)
        {
            var textQuestionAnswersViewModels = textQuestionAnswersDto.Select(GetViewModel).ToList();

            return textQuestionAnswersViewModels;
        }
    }
}
