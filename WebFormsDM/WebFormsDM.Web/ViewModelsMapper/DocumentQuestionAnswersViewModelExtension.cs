﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Web.Models;

namespace WebFormsDM.Web.ViewModelsMapper
{
    public static class DocumentQuestionAnswersViewModelExtension
    {
        public static DocumentQuestionAnswersDto GetDto(this DocumentQuestionAnswersViewModel documentQuestionAnswersViewModel)
        {
   
            var documentQuestionAnswerDto = new DocumentQuestionAnswersDto
            {
                Id = documentQuestionAnswersViewModel.Id,
                DocumentQuestionId = documentQuestionAnswersViewModel.DocumentQuestionId,
                CorrelationToken = documentQuestionAnswersViewModel.CorrelationToken,
                //FileAnswersDto = documentQuestionAnswersViewModel.FileAnswers.GetDtos(),
                AnsewrPaths = documentQuestionAnswersViewModel.AnswerPaths

            };

            return documentQuestionAnswerDto;
        }

        public static List<DocumentQuestionAnswersDto> GetDtos(this List<DocumentQuestionAnswersViewModel> documentQuestionAnswersViewModels)
        {
            var documentQuestionAnswersDtos = documentQuestionAnswersViewModels.Select(GetDto).ToList();

            return documentQuestionAnswersDtos;
        }

        //FROM DTOS TO VIEW MODELS
        public static DocumentQuestionAnswersViewModel GetViewModel(this DocumentQuestionAnswersDto documentQuestionAnswersDto)
        {
            if (documentQuestionAnswersDto == null)
            {
                throw new ArgumentNullException("DocumentQuestionAnswersDto cannot be null");
            }

            return new DocumentQuestionAnswersViewModel
            {
                Id = documentQuestionAnswersDto.Id,
                DocumentQuestionId = documentQuestionAnswersDto.DocumentQuestionId,
                CorrelationToken = documentQuestionAnswersDto.CorrelationToken,
                AnswerPaths = documentQuestionAnswersDto.AnsewrPaths,
                FileAnswers = documentQuestionAnswersDto.FileAnswersDto.GetViewModels()

            };
        }

        public static ICollection<DocumentQuestionAnswersViewModel> GetViewModels(this ICollection<DocumentQuestionAnswersDto> documentQuestionAnswersDtos)
        {
            var documentQuestionAnswersViewModels = documentQuestionAnswersDtos.Select(GetViewModel).ToList();

            return documentQuestionAnswersViewModels;
        }

    }
}
