﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Web.Models;

namespace WebFormsDM.Web.ViewModelsMapper
{
    public static class FormViewModelExtension
    {
        public static FormDto GetDto(this FormViewModel formViewModel)
        {
            var formDto = new FormDto()
            {
                Id = formViewModel.Id,
                Title = formViewModel.Title,
                Description = formViewModel.Description,
                UserId = formViewModel.UserId,
                NumberOfFilledForms = formViewModel.NumberOfFilledForms,
                IsDeleted = formViewModel.IsDeleted,
                CreatedOn = formViewModel.CreatedOn,
                LastModifiedOn = formViewModel.LastModifiedOn,
                DateOfExpiration = formViewModel.DateOfExpiration,
                TextQuestions = formViewModel.TextQuestions.GetDtos(),
                OptionQuestions = formViewModel.OptionQuestions.GetDtos(),
                DocumentQuestions = formViewModel.DocumentQuestions.GetDtos(),
                AllCorrelationTokens = formViewModel.AllCorrelationTokens
            };

            return formDto;
        }

        public static ICollection<FormDto> GetDtos(this ICollection<FormViewModel> formViewModels)
        {
            var formDtos = formViewModels.Select(GetDto).ToList();

            return formDtos;
        }

        //FROM DTOS TO VIEW MODELS
        public static FormViewModel GetViewModel(this FormDto formDto)
        {
            if (formDto == null)
            {
                throw new ArgumentNullException("FormDto cannot be null");
            }

            return new FormViewModel
            {
                Id = formDto.Id,
                Title = formDto.Title,
                Description = formDto.Description,
                UserId = formDto.UserId,
                NumberOfFilledForms = formDto.NumberOfFilledForms,
                IsDeleted = formDto.IsDeleted,
                CreatedOn = formDto.CreatedOn,
                LastModifiedOn = formDto.LastModifiedOn,
                DateOfExpiration = formDto.DateOfExpiration,
                TextQuestions = formDto.TextQuestions.GetViewModels(),
                OptionQuestions = formDto.OptionQuestions.GetViewModels(),
                DocumentQuestions = formDto.DocumentQuestions.GetViewModels(),
                AllCorrelationTokens = formDto.AllCorrelationTokens

            };
        }

        public static ICollection<FormViewModel> GetViewModels(this ICollection<FormDto> formDtos)
        {
            var formViewModels = formDtos.Select(GetViewModel).ToList();

            return formViewModels;
        }

    }
}
