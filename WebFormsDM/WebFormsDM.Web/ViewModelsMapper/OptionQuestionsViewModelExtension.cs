﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Web.Models;

namespace WebFormsDM.Web.ViewModelsMapper
{
    public static class OptionQuestionsViewModelExtension
    {
        //FROM VIEW MODELS TO DTOS

        public static OptionQuestionsDto GetDto(this OptionQuestionsViewModel optionQuestionsVM)
        {
            var optionQuestionsDto = new OptionQuestionsDto
            {
                Id = optionQuestionsVM.Id,
                Description = optionQuestionsVM.Description,
                IsRequired = optionQuestionsVM.IsRequired,
                IsAllowedMultipleAnswers = optionQuestionsVM.IsAllowedMultipleAnswers,
                OrderNumber = optionQuestionsVM.OrderNumber,
                Options = optionQuestionsVM.Options.GetDtos(),
                FormId = optionQuestionsVM.FormId,
               // Answers = optionQuestionsVM.Answers.GetDtos()
            };

            return optionQuestionsDto;
        }

        public static ICollection<OptionQuestionsDto> GetDtos(
            this List<OptionQuestionsViewModel> optionQuestionsVMs)
        {
            return optionQuestionsVMs.Select(GetDto).ToList();
        }

        //FROM DTOS TO VIEW MODELS

        public static OptionQuestionsViewModel GetViewModel(this OptionQuestionsDto optionQuestionsDto)
        {
            var optionQuestionsVM = new OptionQuestionsViewModel
            {
                Id = optionQuestionsDto.Id,
                Description = optionQuestionsDto.Description,
                IsRequired = optionQuestionsDto.IsRequired,
                IsAllowedMultipleAnswers = optionQuestionsDto.IsAllowedMultipleAnswers,
                OrderNumber = optionQuestionsDto.OrderNumber,
                Options = optionQuestionsDto.Options.GetViewModels(),
                FormId = optionQuestionsDto.FormId,
                Answers = optionQuestionsDto.Answers.GetEntities()
            };

            return optionQuestionsVM;
        }

        public static List<OptionQuestionsViewModel> GetViewModels(
            this ICollection<OptionQuestionsDto> optionQuestionsDtos)
        {
            return optionQuestionsDtos.Select(GetViewModel).ToList();
        }
    }
}

