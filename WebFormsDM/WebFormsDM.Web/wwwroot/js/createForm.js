﻿let masterCouter = 0;
let counter = 0;
$('#btnAddTextQ').on('click', () => {
    counter++
    masterCouter++
    let div = document.getElementById('questionContainer')
    let element =
        `<div class="col-sm-12" id="QuestionID${masterCouter}">
                            <div class="col-sm-11">
                                <input type="text" class="form-control" id="textQuestionDescription${counter}" placeholder="Enter your question">
                                <input type="hidden" id="OrderNumber${counter}" name="OrderNumber" value="${masterCouter}">
                                <div class="col-sm-12">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="IsLongAnswerTextQuestion${counter}">
                                        <label class="custom-control-label" for="IsLongAnswerTextQuestion${counter}">Long answer</label>
                                    </div>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="IsRequiredTextQuestion${counter}">
                                        <label class="custom-control-label" for="IsRequiredTextQuestion${counter}">Required</label>
                                    </div>
                                </div>
                                <div>
                                    <div class="col-sm-4" id="counter">
                                        <button onclick="removeQ(this.id)" class="btn btn-info" id="${masterCouter}"> Remove </button>
                                    </div>
                                </div>
                                <br />
                            </div>
                        </div>`
    $(div).append(element)
})


function removeQ(id) {
    let divToRomove = "QuestionID" + id;
    let element = document.getElementById(divToRomove)
    element.remove()
}

let counterDocumentQ = 0;
$('#btnAddDocumentQ').on('click', () => {
    counterDocumentQ++
    masterCouter++
    let div = document.getElementById('questionContainer')
    let element = `<div class="col-sm-12" id="QuestionID${masterCouter}">
                          <div class="col-sm-11">
                              <input type="text" class="form-control" id="documentQuestionDescription${counterDocumentQ}" placeholder="Enter your question">
                              <input type="hidden" id="OrderNumberDQ${counterDocumentQ}" name="OrderNumber" value="${masterCouter}">
                              <div class="col-sm-12">
                                  <div class="custom-control custom-switch">
                                      <input type="checkbox" class="custom-control-input" id="IsRequiredDocumentQuestion${counterDocumentQ}">
                                      <label class="custom-control-label" for="IsRequiredDocumentQuestion${counterDocumentQ}">Required</label>
                                  </div>
                              </div>
                              <div class="col-sm-12">
                                  <div class="dropdown pull-left" id="NumberOfFiles${counterDocumentQ}">
                                      <label class="control-label">
                                          Number Of Files
                                          <select class="form-control">
                                              <option value="1">1</option>
                                              <option value="2">2</option>
                                              <option value="3">3</option>
                                              <option value="4">4</option>
                                              <option value="5">5</option>
                                              <option value="6">6</option>
                                              <option value="7">7</option>
                                              <option value="8">8</option>
                                              <option value="9">9</option>
                                              <option value="10">10</option>
                                          </select>
                                      </label>
                                  </div>
                              </div>
                              <div class="col-sm-12">
                                  <div class="dropdown pull-right" id="FileSize${counterDocumentQ}">
                                      <label class="control-label">
                                          File Size
                                          <select class="form-control">
                                              <option value="1">1MB</option>
                                              <option value="10">10MB</option>
                                              <option value="100">100MB</option>
                                          </select>
                                      </label>
                                  </div>
                              </div>
                              <div>
                                  <div class="col-sm-4" id="counter">
                                      <button onclick="removeQ(this.id)" class="btn btn-info" id="${masterCouter}"> Remove </button>
                                  </div>
                              </div>
                              <br />
                          </div>
                       </div>`
    $(div).append(element)
})

let counterOptionQuestion = 0;
$('#btnAddOptionQ').on('click', () => {
    counterOptionQuestion++
    masterCouter++
    counterOptions++
    counterOptions++
    let div = document.getElementById('questionContainer')
    let element = `<div class="col-sm-12" id="QuestionID${masterCouter}">
                            <div class="col-sm-11">
                                <input type="text" class="form-control" id="optionQuestionDescription${counterOptionQuestion}" placeholder="Enter your question">
                                <input type="hidden" id="OrderNumberOQ${counterOptionQuestion}" name="OrderNumber" value="${masterCouter}">
                                <div class="col-sm-12">
                                    <div class="custom-control custom-switch">
                                        <input asp-for="IsRequired" type="checkbox" class="custom-control-input" id="IsRequiredOptionQuestion${counterOptionQuestion}">
                                        <label class="custom-control-label" for="IsRequiredOptionQuestion${counterOptionQuestion}">Required</label>
                                        <span asp-validation-for="IsRequired" class="text-danger"></span>
                                    </div>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="IsAllowedMultipleAnswers${counterOptionQuestion}">
                                        <label class="custom-control-label" for="IsAllowedMultipleAnswers${counterOptionQuestion}">Allowed Multiple Answers</label>
                                    </div>
                                    <div class="col-sm-11">
                                        <div class="col-sm-11" id="add${counterOptionQuestion}">
                                        <div id="optionID${counterOptions - 1}" class="row">
                                       <input type="text" id="option${counterOptions - 1}" placeholder="Enter your option">
                                 </div>
                                 <div id="optionID${counterOptions}" class="row">
                                       <input type="text" id="option${counterOptions}" placeholder="Enter your option">
                                 </div>
                                            <span style="color:red" id="message${counterOptionQuestion}"> </span>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div>
                                    <div class="col-sm-4" id="counter">
                                        <button onclick="addFunction(this.id)" class="btn btn-outline-info" id="${counterOptionQuestion}"> Add Option </button>
                                    </div>
                                </div>
                                <br />
                                <div>
                                    <div class="col-sm-4" id="counter">
                                        <button onclick="removeQ(this.id)" class="btn btn-info" id="${masterCouter}"> Remove </button>
                                    </div>
                                </div>
                                <br />
                            </div>
                        </div>`
    $(div).append(element)
})

let counterOptions = 0;
function addFunction(id) {
    counterOptions++
    let divConteiner = "add" + id;
    let div = document.getElementById(divConteiner)
    let element = `<div id="optionID${counterOptions}" class="row">
                                       <input type="text" id="option${counterOptions}" placeholder="Enter your option">
                                       <div class="col-sm-4" id="counter">
                                        <button onclick="removeOption(this.id)" class="btn btn-outline-info" id="${counterOptions}"> Remove</button>
                                    </div>
                                 </div>`
    $(div).append(element)
}

function removeOption(id) {
    let divToRomove = "optionID" + id;
    let element = document.getElementById(divToRomove)
    element.remove()
}

function create() {
    let title = document.getElementById("formTitle").value;
    let description = document.getElementById("formDescription").value;
    let textQuestionList = new Array();
    let optionQuestionlist = new Array();
    let documentQuestionList = new Array();

    for (let i = 1; i <= counter; i++) {

        let modelTextQuestionIsRequired = false;
        let modelTextQuestionIsLongAnswer = false;

        if ((document.getElementById('textQuestionDescription' + i.toString())) === null) {
            continue;
        }
        else {
            let questionDescription = document.getElementById('textQuestionDescription' + i.toString()).value;
            let orderNumber = document.getElementById('OrderNumber' + i.toString()).value;
            let islongAnswer = document.getElementById('IsLongAnswerTextQuestion' + i.toString());
            let isRequired = document.getElementById('IsRequiredTextQuestion' + i.toString());

            if (islongAnswer.checked) {
                modelTextQuestionIsLongAnswer = true;
            }
            if (isRequired.checked) {
                modelTextQuestionIsRequired = true;
            }
            let TextQuestionViewModel = {}

            TextQuestionViewModel.Description = questionDescription;
            TextQuestionViewModel.IsLongAnswer = modelTextQuestionIsLongAnswer;
            TextQuestionViewModel.IsRequired = modelTextQuestionIsRequired;
            TextQuestionViewModel.OrderNumber = orderNumber;

            textQuestionList.push(TextQuestionViewModel);
        }
    }

    for (let j = 1; j <= counterOptionQuestion; j++) {

        let modelOptionQuestionIsRequired = false;
        let modelIsAllowedMultipleAnswer = false;

        if ((document.getElementById('optionQuestionDescription' + j.toString())) === null) {
            continue;
        }
        else {
            let optionQuestionDescription = document.getElementById('optionQuestionDescription' + j.toString()).value;
            let orderNumberOptionQ = document.getElementById('OrderNumberOQ' + j.toString()).value;
            let isRequiredOptionQuestion = document.getElementById('IsRequiredOptionQuestion' + j.toString());
            let isAllowedMultipleAnswers = document.getElementById('IsAllowedMultipleAnswers' + j.toString());


            let optionsInfo = $(`#add${j} :input`);

            console.log(optionsInfo);
            console.log(optionsInfo.length);

            if (optionsInfo.length < 2) {
                $(`#message${j}`).text("Add at least 2 options");
            }
            else {
                $(`#message${j}`).text("");
            }

            let Options = new Array()


            for (var k = 0; k < optionsInfo.length; k++) {

                let optionsQuestionOption = optionsInfo[k].value;
                console.log(optionsQuestionOption);
                Options.push(optionsQuestionOption);
            }

            if (isRequiredOptionQuestion.checked) {
                modelOptionQuestionIsRequired = true;
            }

            if (isAllowedMultipleAnswers.checked) {
                modelIsAllowedMultipleAnswer = true;
            }

            let OptionQuestionsViewModel = {}

            OptionQuestionsViewModel.Description = optionQuestionDescription;
            OptionQuestionsViewModel.IsRequired = modelOptionQuestionIsRequired;
            OptionQuestionsViewModel.OptionsAsString = Options;
            OptionQuestionsViewModel.IsAllowedMultipleAnswers = modelIsAllowedMultipleAnswer;
            OptionQuestionsViewModel.OrderNumber = orderNumberOptionQ;

            optionQuestionlist.push(OptionQuestionsViewModel);
        }
    }

    for (let m = 1; m <= counterDocumentQ; m++) {

        let modelDocumentQuestionIsRequired = false;

        if ((document.getElementById('documentQuestionDescription' + m.toString())) === null) {
            continue;
        }
        else {
            let documentQuestionDescription = document.getElementById('documentQuestionDescription' + m.toString()).value;
            let orderNumberDocumentQ = document.getElementById('OrderNumberDQ' + m.toString()).value;
            let documentQuestionIsRequired = document.getElementById('IsRequiredDocumentQuestion' + m.toString());
            var dQNumberOfFiles = $('#NumberOfFiles' + m.toString()).find(":selected").text();
            var dQFileSize = $('#FileSize' + m.toString()).find(":selected").text();

            if (documentQuestionIsRequired.checked) {
                modelDocumentQuestionIsRequired = true;
            }
            let DocumentQuestionViewModel = {}

            DocumentQuestionViewModel.Description = documentQuestionDescription;
            DocumentQuestionViewModel.IsRequired = modelDocumentQuestionIsRequired;
            DocumentQuestionViewModel.NumberOfFiles = dQNumberOfFiles;
            DocumentQuestionViewModel.MaxFileSize = dQFileSize;
            DocumentQuestionViewModel.OrderNumber = orderNumberDocumentQ;

            documentQuestionList.push(DocumentQuestionViewModel);
        }
    }


    let TextQuestions = JSON.stringify(textQuestionList);
    let OptionQuestions = JSON.stringify(optionQuestionlist);
    let DocumentQuestions = JSON.stringify(documentQuestionList);
    console.log(title);
    console.log(description);

    $.ajax({
        type: 'Post',
        url: 'Create',
        data: {
            'Title': title,
            'Description': description,
            'TextQuestions': textQuestionList,
            'OptionQuestions': optionQuestionlist,
            'DocumentQuestions': documentQuestionList
        },
        success: function () {
            window.location.replace("/Forms/Index")

        }
    })
}