﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebFormsDM.Web.Models
{
    public class TextQuestionViewModel
    {
        public TextQuestionViewModel()
        {
            this.Answers = new List<TextQuestionAnswersViewModel>();
        }
        public Guid Id { get; set; }
        [Required]
        public string Description { get; set; }
        public bool IsLongAnswer { get; set; }
        public bool IsRequired { get; set; }
        public Guid FormId { get; set; }
        public string FormTitle { get; set; }
        public int OrderNumber { get; set; }
        public ICollection<TextQuestionAnswersViewModel> Answers { get; set; }
    }

}

