﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebFormsDM.Web.Models
{
    public class DocumentQuestionViewModel
    {
        public DocumentQuestionViewModel()
        {
           this.Answers = new List<DocumentQuestionAnswersViewModel>();
        }
        public Guid Id { get; set; }
        [Required]
        public string Description { get; set; }
        public bool IsRequired { get; set; }
        public string MaxFileSize { get; set; }
        public int NumberOfFiles { get; set; }
        public Guid FormId { get; set; }
        public int OrderNumber { get; set; }
        public bool IsDeleted { get; set; }
        public ICollection<DocumentQuestionAnswersViewModel> Answers { get; set; }



    }
}
