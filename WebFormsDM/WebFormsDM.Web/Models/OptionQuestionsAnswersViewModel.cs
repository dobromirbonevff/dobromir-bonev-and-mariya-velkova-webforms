﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebFormsDM.Web.Models
{
    public class OptionQuestionsAnswersViewModel
    {
        public OptionQuestionsAnswersViewModel()
        {
            this.Options = new List<OptionsQuestionOptionViewModel>();
            this.OptionsAsString = new List<string>();
        }
        public Guid Id { get; set; }
        public Guid OptionsQuestionId { get; set; }
        public string OptionsQuestionDescription { get; set; }
        public Guid CorrelationToken { get; set; }
        public List<OptionsQuestionOptionViewModel> Options { get; set; }
        public List<string> OptionsAsString { get; set; }
        public string Answer { get; set; }
    }
}
