﻿using System;

namespace WebFormsDM.Web.Models
{
    public class FileAnswersViewModel
    {
        public Guid Id { get; set; }
        public string FilePath { get; set; }
        public Guid DocumentQAnswerId { get; set; }
       // public DocumentQuestionAnswersViewModel DocumentQuestionAswer { get; set; }
    }
}