﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebFormsDM.Web.Models
{
    public class TextQuestionAndAnswersViewModel
    {
        public TextQuestionViewModel TextQuestionViewModel { get; set; }
        public TextQuestionAnswersViewModel TextQuestionAnswersViewModel { get; set; }
        
    }
}
