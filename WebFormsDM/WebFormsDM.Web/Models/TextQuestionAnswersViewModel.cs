﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebFormsDM.Web.Models
{
    public class TextQuestionAnswersViewModel
    {
        public Guid Id { get; set; }
        public string Answer { get; set; }
        public Guid TextQuestionId { get; set; }
      //  public TextQuestionViewModel TextQuestion { get; set; }
        public Guid CorrelationToken { get; set; }
    }
}
