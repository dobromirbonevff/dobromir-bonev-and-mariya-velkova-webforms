﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebFormsDM.Web.Models
{
    public class DocumentQuestionAndAnswersViewModel
    {
        public DocumentQuestionViewModel DocumentQuestionViewModel { get; set; }
        public DocumentQuestionAnswersViewModel DocumentQuestionAnswersViewModel { get; set; }
    }
}
