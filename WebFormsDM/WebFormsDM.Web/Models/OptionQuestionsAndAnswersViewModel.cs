﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebFormsDM.Web.Models
{
    public class OptionQuestionsAndAnswersViewModel
    {
        public OptionQuestionsViewModel OptionQuestionsViewModel { get; set; }
        public OptionQuestionsAnswersViewModel OptionQuestionsAnswersViewModel { get; set; }
    }
}
