﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebFormsDM.Web.Models
{
    public class FillFormViewModel
    {
        
        public FillFormViewModel()
        {

            this.TextQuestionAndAnswersViewModels = new List<TextQuestionAndAnswersViewModel>();
            this.DocumentQuestionsAndAnswers = new List<DocumentQuestionAndAnswersViewModel>();
            this.OptionQuestionsAndAnswersViewModels = new List<OptionQuestionsAndAnswersViewModel>(); 
            
        }
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int AllQuestionsCount { get; set; }
        public List<TextQuestionAndAnswersViewModel> TextQuestionAndAnswersViewModels { get; set; }
        public List<DocumentQuestionAndAnswersViewModel> DocumentQuestionsAndAnswers { get; set; }
        public List<OptionQuestionsAndAnswersViewModel> OptionQuestionsAndAnswersViewModels { get; set; }



    }
}
