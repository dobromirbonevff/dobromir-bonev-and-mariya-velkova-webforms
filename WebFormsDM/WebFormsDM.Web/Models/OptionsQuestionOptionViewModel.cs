﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebFormsDM.Web.Models
{
    public class OptionsQuestionOptionViewModel
    {
        public OptionsQuestionOptionViewModel()
        {
            
        }
        public Guid Id { get; set; }
        public string Option { get; set; }
        public Guid OptionsQuestionId { get; set; }
        public bool IsSelected { get; set; }
       // public OptionQuestionsAnswersViewModel Answer { get; set; }
    }
}
