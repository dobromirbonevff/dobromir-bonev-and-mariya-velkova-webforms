﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebFormsDM.Web.Models
{
    public class FormViewModel
    {
        public FormViewModel()
        {
            this.TextQuestions = new List<TextQuestionViewModel>();
            this.OptionQuestions = new List<OptionQuestionsViewModel>();
            this.DocumentQuestions = new List<DocumentQuestionViewModel>();
            this.AllCorrelationTokens = new List<Guid>();
            //this.Questions = new Questions();
        }
        public Guid Id { get; set; }   
        [Required]
        [MaxLength(150)]
        public string Title { get; set; }
        public string Description { get; set; }
        public int NumberOfFilledForms { get; set; }
        public Guid UserId { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
        public DateTime DateOfExpiration { get; set; }
        public DateTime? LastModifiedOn { get; set; } = DateTime.UtcNow;
        public bool IsDeleted { get; set; }
        
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string RecipientOfTheSurvey { get; set; }

        //public User User { get; set; }
        public List<TextQuestionViewModel> TextQuestions { get; set; }
        public List<OptionQuestionsViewModel> OptionQuestions { get; set; }
        public List<DocumentQuestionViewModel> DocumentQuestions { get; set; }
        public ICollection<Guid> AllCorrelationTokens { get; set; }
        //public Questions Questions { get; set; }
    }

}

