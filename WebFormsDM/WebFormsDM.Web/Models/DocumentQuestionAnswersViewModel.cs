﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebFormsDM.Web.Models
{
    public class DocumentQuestionAnswersViewModel
    {
        public DocumentQuestionAnswersViewModel()
        {
            this.Files = new List<IFormFile>();
            this.AnswerPaths= new List<string>();
            this.FileAnswers = new List<FileAnswersViewModel>();

        }
 
        public Guid Id { get; set; }
        public Guid DocumentQuestionId { get; set; }
        //public DocumentQuestionViewModel DocumentQuestion { get; set; }
        public Guid CorrelationToken { get; set; }
        public ICollection<FileAnswersViewModel> FileAnswers { get; set; }
        public List<string> AnswerPaths { get; set; }
      
        public List<IFormFile> Files { get; set; }
    }
}
