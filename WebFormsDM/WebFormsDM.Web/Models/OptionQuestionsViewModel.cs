﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebFormsDM.Web.Models
{
    public class OptionQuestionsViewModel
    {
        public OptionQuestionsViewModel()
        {
            this.Options = new List<OptionsQuestionOptionViewModel>();
            this.OptionsAsString = new List<string>();
            this.Answers = new List<OptionQuestionsAnswersViewModel>();
        }
        public Guid Id { get; set; }
        [Required]
        public string Description { get; set; }
        public bool IsRequired { get; set; }
        public bool IsAllowedMultipleAnswers { get; set; }
        public int OrderNumber { get; set; }
        public Guid FormId { get; set; }
        public List<OptionsQuestionOptionViewModel> Options { get; set; }
        public ICollection<string> OptionsAsString { get; set; }
        public ICollection<OptionQuestionsAnswersViewModel> Answers { get; set; }
    }
}
