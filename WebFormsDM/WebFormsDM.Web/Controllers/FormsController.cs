﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.Contracts;
using WebFormsDM.Web.Models;
using WebFormsDM.Web.ViewModelsMapper;

namespace WebFormsDM.Web.Controllers
{
    public class FormsController : Controller
    {
        private readonly IFormService formServices;
        private readonly UserManager<User> userManager;
        private readonly IToastNotification toastNotification;

        public FormsController(IFormService formServices,
                               UserManager<User> userManager,
                               IToastNotification toastNotification)
        {
            this.formServices = formServices ?? throw new ArgumentNullException(nameof(formServices));
            this.userManager = userManager;
            this.toastNotification = toastNotification;
        }

        [HttpGet]
        public IActionResult Create()
        {
            var formViewModel = new FormViewModel();
            return View(formViewModel);
        }
        [HttpPost]
        public async Task Create(FormViewModel formViewModel)
        {
            if (!ModelState.IsValid)
            {
                //this.toastNotification.AddWarningToastMessage($"Something went wrong!");
            }
            try
            {
                formViewModel.UserId = (await userManager.GetUserAsync(User)).Id;

                foreach (var optionsQuestion in formViewModel.OptionQuestions)
                {
                    foreach (var option in optionsQuestion.OptionsAsString)
                    {
                        if(option != null)
                        {
                            var oQ = new OptionsQuestionOptionViewModel();
                            oQ.Option = option;
                            optionsQuestion.Options.Add(oQ);
                        }
                    }
                }
               var formDto = formViewModel.GetDto();

               var newForm = await this.formServices.CreateFormAsync(formDto);

                this.toastNotification.AddWarningToastMessage($"The form has been created!");
            }
            catch (Exception)
            {
                this.toastNotification.AddWarningToastMessage($"Something went wrong!");
            }
        }

        [HttpGet]
        public async Task<IActionResult> Edit(Guid id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var formDto = await this.formServices.GetFormAsync(id);

                if (formDto == null)
                {
                    return NotFound();
                }

                var formViewModel = formDto.GetViewModel();


                return View(formViewModel);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task Edit(FormViewModel formViewModel)
        {
  
            try
            {
                formViewModel.UserId = (await userManager.GetUserAsync(User)).Id;

                foreach (var optionsQuestion in formViewModel.OptionQuestions)
                {
                    foreach (var option in optionsQuestion.OptionsAsString)
                    {
                        if (option != null)
                        {
                            var oQ = new OptionsQuestionOptionViewModel();
                            oQ.Option = option;
                            optionsQuestion.Options.Add(oQ);
                        }
                    }
                }
                var formDto = formViewModel.GetDto();

                var newForm = await this.formServices.CreateNewVersionOfFormAsync(formDto);

                var newformViewModel = newForm.GetViewModel();


            }
            catch (Exception)
            {
                this.toastNotification.AddWarningToastMessage($"Something went wrong!");
            }

        }
        [HttpGet]
        public async Task<IActionResult> GetFormWithAnswers(Guid id)
        {
            var formDto = await this.formServices.GetFormWithAnswersAsync(id);
            var formVM = formDto.GetViewModel();

            return View(formVM);
        }

        [HttpGet]
        public async Task<IActionResult> GetFormWithOneAnswer(Guid formId, Guid correlationToken)
        {
            var formDto = await this.formServices.GetFromWithOneAnswerAsync(formId, correlationToken);
            var fromVM = formDto.GetViewModel();

            return View(fromVM);
        }
    

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var userId = (await userManager.GetUserAsync(User)).Id;
            var allUserForms = await this.formServices.GetUserFormsAsync(userId);
            var allUserFormsVM = allUserForms.GetViewModels();

            return View(allUserFormsVM);
        } 

    }

}