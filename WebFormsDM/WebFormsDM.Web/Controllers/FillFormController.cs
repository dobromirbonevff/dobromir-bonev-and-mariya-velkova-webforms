﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using WebFormsDM.Services.Contracts;
using WebFormsDM.Web.Models;
using WebFormsDM.Web.ViewModelsMapper;

namespace WebFormsDM.Web.Controllers
{
    public class FillFormController : Controller
    {
        private readonly IFillFormService fillFormServices;
        private readonly IWebHostEnvironment webHostEnvironment;
        private readonly IToastNotification toastNotification;

        public FillFormController(IFillFormService fillFormServices,
                                  IWebHostEnvironment webHostEnvironment,
                                  IToastNotification toastNotification)

        {
            this.fillFormServices = fillFormServices;
            this.webHostEnvironment = webHostEnvironment;
            this.toastNotification = toastNotification;
        }


        [HttpGet]
        public async Task<IActionResult> Fill(Guid id)
        {

            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var fillFormDto = await this.fillFormServices.GetFillFormAsync(id);

                if (fillFormDto == null)
                {
                    return NotFound();
                }

                var fillFormViewModel = fillFormDto.GetViewModel();


                return View(fillFormViewModel);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Fill(FillFormViewModel fillFormViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    foreach (var item in fillFormViewModel.DocumentQuestionsAndAnswers)
                    {
                        if (item.DocumentQuestionViewModel.IsRequired == true)
                        {
                            if (item.DocumentQuestionAnswersViewModel.Files == null || item.DocumentQuestionAnswersViewModel.Files.Count == 0)
                            {
                                this.toastNotification.AddWarningToastMessage($"Please fill out the survey correctly");
                                return RedirectToAction("Fill", "FillForm", fillFormViewModel.Id);
                            }
                        }
                        var sizeLimit = int.Parse(item.DocumentQuestionViewModel.MaxFileSize.Split("MB").First());
                        var sizeLimitInBytes = sizeLimit * 1024 * 1024;

                        if (item.DocumentQuestionAnswersViewModel.Files.Count > item.DocumentQuestionViewModel.NumberOfFiles)
                        {
                            this.toastNotification.AddWarningToastMessage($"Please fill out the survey correctly");
                            return RedirectToAction("Fill", "FillForm", fillFormViewModel.Id);
                        }
                        foreach (var file in item.DocumentQuestionAnswersViewModel.Files)
                        {
                            if (file.Length <= sizeLimitInBytes)
                            {
                                var newFileName = $"{Guid.NewGuid()}_{file.FileName}";
                                string fileDBPath = $"/Files/{newFileName}";

                                item.DocumentQuestionAnswersViewModel.AnswerPaths.Add(fileDBPath);

                                var saveFile = Path.Combine(webHostEnvironment.WebRootPath, "Files", newFileName);
                                {
                                    var fileSelected = new FileStream(saveFile, FileMode.Create);
                                    await file.CopyToAsync(fileSelected);
                                }
                            }
                        }
                    }
                    foreach (var item in fillFormViewModel.OptionQuestionsAndAnswersViewModels)
                    {

                        if (item.OptionQuestionsViewModel.IsAllowedMultipleAnswers == true)
                        {
                            //multiple answers
                            if (item.OptionQuestionsViewModel.IsRequired == true)
                            {
                                if (item.OptionQuestionsViewModel.Options == null || item.OptionQuestionsViewModel.Options.Count == 0)
                                {
                                    this.toastNotification.AddWarningToastMessage($"Please fill out the survey correctly");
                                    return RedirectToAction("Fill", "FillForm", fillFormViewModel.Id);
                                }
                            }
                            var answer = "";
                            foreach (var option in item.OptionQuestionsViewModel.Options)
                            {

                                if (option.IsSelected == true)
                                {
                                    option.OptionsQuestionId = item.OptionQuestionsAnswersViewModel.OptionsQuestionId;
                                    answer += option.Option + ",";

                                }
                            }
                            item.OptionQuestionsAnswersViewModel.Answer = answer;
                        }
                        // only one answer
                        else
                        {
                            var answer = item.OptionQuestionsAnswersViewModel.OptionsAsString;


                            if (answer == null || answer.Count == 0)
                            {
                                item.OptionQuestionsAnswersViewModel.Answer = "NA";
                            }
                            else
                            {
                                var answerOption = answer.First();
                                item.OptionQuestionsAnswersViewModel.Answer = answerOption;
                            }

                        }

                    }


                    var fillFormDto = fillFormViewModel.GetDto();

                    fillFormDto = await this.fillFormServices.CreateFormAnswersAsync(fillFormDto);


                    var newFillFormVM = fillFormDto.GetViewModel();


                    return RedirectToAction("SuccessfullyCompleted", newFillFormVM);

                }
            }

            catch (Exception)
            {
                this.toastNotification.AddWarningToastMessage($"Please fill out the survey correctly");
                return RedirectToAction("Fill", "FillForm", fillFormViewModel.Id);
            }
            return View();


        }
        [HttpGet]
        public ViewResult SuccessfullyCompleted(FillFormViewModel model)
        {
            return View(model);
        }



    }

}