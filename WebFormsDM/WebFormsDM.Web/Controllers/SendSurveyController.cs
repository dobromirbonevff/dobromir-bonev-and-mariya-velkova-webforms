﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using WebFormsDM.Web.Models;

namespace WebFormsDM.Web.Controllers
{
    public class SendSurveyController : Controller
    {
        private readonly IToastNotification toastNotification;

        public SendSurveyController(IToastNotification toastNotification)
        {
            this.toastNotification = toastNotification;
        }

        public IActionResult SendSurveyByEmail(FormViewModel form)
        {
            try
            {
                string sendTo = form.RecipientOfTheSurvey;
                MailMessage mail = new MailMessage();
                mail.To.Add(sendTo);
                mail.Subject = "Survey";
                mail.Body = "https://localhost:44342/FillForm/Fill/" + form.Id;
                mail.From = new MailAddress("dobromirbonevff@gmail.com");
                mail.IsBodyHtml = false;
                SmtpClient smtp = new SmtpClient("smtp.gmail.com");
                smtp.Port = 587;
                smtp.UseDefaultCredentials = true;
                smtp.EnableSsl = true;
                smtp.Credentials = new System.Net.NetworkCredential("webformsdm@gmail.com", "WebForms1");
                smtp.Send(mail);
            }
            catch (Exception)
            {
                this.toastNotification.AddWarningToastMessage($"Something went wrong!");
                return Redirect("/Forms/Index");
            }


            this.toastNotification.AddInfoToastMessage($"The survey was successfully sent");
            return Redirect("/Forms/Index");
        }
    }
}