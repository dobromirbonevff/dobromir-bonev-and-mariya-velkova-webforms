﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFormsDM.Services.DtoEntities
{
    public class TextQuestionAndAnswersDto
    {
        public TextQuestionDto TextQuestionDto { get; set; }
        public TextQuestionAnswersDto TextQuestionAnswersDto { get; set; }
    }
}
