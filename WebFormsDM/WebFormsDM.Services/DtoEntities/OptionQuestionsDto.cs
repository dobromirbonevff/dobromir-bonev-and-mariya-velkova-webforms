﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFormsDM.Services.DtoEntities
{
    public class OptionQuestionsDto
    {
        public OptionQuestionsDto()
        {
            this.Options = new List<OptionsQuestionOptionDto>();
            this.Answers = new List<OptionQuestionsAnswersDto>();
        }
        public Guid Id { get; set; }
        public string Description { get; set; }
        public bool IsRequired { get; set; }
        public bool IsAllowedMultipleAnswers { get; set; }
        public int OrderNumber { get; set; }
        public Guid FormId { get; set; }
        public ICollection<OptionsQuestionOptionDto> Options { get; set; }
        public ICollection<OptionQuestionsAnswersDto> Answers { get; set; }
    }
}
