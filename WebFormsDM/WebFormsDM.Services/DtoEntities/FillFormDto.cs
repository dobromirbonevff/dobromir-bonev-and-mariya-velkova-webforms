﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFormsDM.Services.DtoEntities
{
    public class FillFormDto
    {    
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public ICollection<TextQuestionAndAnswersDto> TextQuestionAndAnswerDtos { get; set; } = new List<TextQuestionAndAnswersDto>();
        public ICollection<DocumentQuestionAndAnswersDto> DocumentQuestionAndAnswerDto { get; set; } = new List<DocumentQuestionAndAnswersDto>();
        public ICollection<OptionQuestionsAndAnswersDto> OptionQuestionsAndAnswersDtos { get; set; } = new List<OptionQuestionsAndAnswersDto>();
        public int AllQuestionsCount { get; set; }
    }
}
