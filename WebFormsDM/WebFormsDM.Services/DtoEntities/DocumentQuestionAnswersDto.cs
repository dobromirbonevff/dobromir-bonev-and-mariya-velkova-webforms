﻿using System;
using System.Collections.Generic;
using System.Text;
using WebFormsDM.Data.Entities;

namespace WebFormsDM.Services.DtoEntities
{
    public class DocumentQuestionAnswersDto
    {
        public DocumentQuestionAnswersDto()
        {
            this.FileAnswersDto = new List<FileAnswersDto>();
            this.AnsewrPaths = new List<string>();
         
        }
        public Guid Id { get; set; }   
        public Guid DocumentQuestionId { get; set; }
        public DocumentQuestion DocumentQuestion { get; set; }
        public Guid CorrelationToken { get; set; }
        public List<string> AnsewrPaths { get; set; }
        public ICollection<FileAnswersDto> FileAnswersDto { get; set; }
    }
}
