﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFormsDM.Services.DtoEntities
{
    public class TextQuestionDto
    {
        public TextQuestionDto()
        {
            this.Answers = new List<TextQuestionAnswersDto>();
        }
        public Guid Id { get; set; }
        public string Description { get; set; }
        public bool IsLongAnswer { get; set; }
        public bool IsRequired { get; set; }
        public Guid FormId { get; set; }
        public string FormTitle { get; set; }
        public int OrderNumber { get; set; }

       public ICollection<TextQuestionAnswersDto> Answers { get; set; }
    }
}
