﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFormsDM.Services.DtoEntities
{
    public class FileAnswersDto
    {
        public Guid Id { get; set; }
        public string FilePath { get; set; }
        public Guid DocumentQAnswerId { get; set; }
       
    }
}
