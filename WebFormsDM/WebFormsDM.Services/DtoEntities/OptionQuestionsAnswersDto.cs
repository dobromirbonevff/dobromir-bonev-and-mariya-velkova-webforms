﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFormsDM.Services.DtoEntities
{
    public class OptionQuestionsAnswersDto
    {
        public OptionQuestionsAnswersDto()
        {
            this.Options = new List<OptionsQuestionOptionDto>();
        }
        public Guid Id { get; set; }
        public Guid OptionsQuestionId { get; set; }
        public string OptionsQuestionDescription { get; set; }
        public Guid CorrelationToken { get; set; }
        public ICollection<OptionsQuestionOptionDto> Options { get; set; }
        public string Answer { get; set; }
    }
}
