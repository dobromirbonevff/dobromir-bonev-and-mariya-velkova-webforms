﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFormsDM.Services.DtoEntities
{
    public class DocumentQuestionDto
    {
        public DocumentQuestionDto()
        {
            this.Answers = new List<DocumentQuestionAnswersDto>();
        }
        public Guid Id { get; set; }
        public string Description { get; set; }
        public bool IsRequired { get; set; }
        public Guid FormId { get; set; }      
        public int OrderNumber { get; set; }
        public bool IsDeleted { get; set; }
        public int FileSize { get; set; }
        public int NumberOfFiles { get; set; }
        public ICollection<DocumentQuestionAnswersDto> Answers { get; set; }
    }
}
