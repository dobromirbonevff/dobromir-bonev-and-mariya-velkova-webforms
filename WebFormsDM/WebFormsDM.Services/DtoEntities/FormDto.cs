﻿using System;
using System.Collections.Generic;
using System.Text;
using WebFormsDM.Data.Entities;

namespace WebFormsDM.Services.DtoEntities
{
    public class FormDto
    {

        public Guid Id { get; set; }
        public DateTime CreatedOn { get; set; } 
        public DateTime DateOfExpiration { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public bool IsDeleted { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int NumberOfFilledForms { get; set; }
        public Guid UserId { get; set; }
        public ICollection<TextQuestionDto> TextQuestions { get; set; }
        public ICollection<OptionQuestionsDto> OptionQuestions { get; set; }
        public ICollection<DocumentQuestionDto> DocumentQuestions { get; set; }
        public ICollection<Guid> AllCorrelationTokens { get; set; }
    }
}
