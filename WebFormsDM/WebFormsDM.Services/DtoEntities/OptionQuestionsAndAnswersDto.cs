﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFormsDM.Services.DtoEntities
{
    public class OptionQuestionsAndAnswersDto
    {
        public OptionQuestionsDto OptionQuestionsDto { get; set; }
        public OptionQuestionsAnswersDto OptionQuestionsAnswersDto { get; set; }
    }
}
