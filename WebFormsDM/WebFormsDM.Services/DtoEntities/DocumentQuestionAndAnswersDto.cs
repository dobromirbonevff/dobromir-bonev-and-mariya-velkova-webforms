﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFormsDM.Services.DtoEntities
{
    public  class DocumentQuestionAndAnswersDto
    {
        public DocumentQuestionDto DocumentQuestionDto { get; set; }
        public DocumentQuestionAnswersDto DocumentQuestionAnswersDto { get; set; }
    }
}
