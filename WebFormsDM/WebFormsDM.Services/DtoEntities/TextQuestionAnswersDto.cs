﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFormsDM.Services.DtoEntities
{
   public class TextQuestionAnswersDto
    {
        public Guid Id { get; set; }
        public string Answer { get; set; }
        public Guid TextQuestionId { get; set; }
       // public TextQuestionDto TextQuestion { get; set; }
        public Guid CorrelationToken { get; set; }
    }
}
