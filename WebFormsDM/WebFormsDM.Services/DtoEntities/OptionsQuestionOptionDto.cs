﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFormsDM.Services.DtoEntities
{
    public class OptionsQuestionOptionDto
    {
        public OptionsQuestionOptionDto()
        {
           
        }
        public Guid Id { get; set; }
        public string Option { get; set; }
        public Guid OptionsQuestionId { get; set; }
       // public OptionQuestionsAnswersDto Answer { get; set; }
    }
}
