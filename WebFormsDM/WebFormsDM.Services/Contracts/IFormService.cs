﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebFormsDM.Services.DtoEntities;

namespace WebFormsDM.Services.Contracts
{
    public interface IFormService
    {
        Task<FormDto> CreateFormAsync(FormDto formDto);
        Task<FormDto> CreateNewVersionOfFormAsync(FormDto formDto);
        Task<FormDto> GetFormAsync(Guid? id);
        Task<ICollection<FormDto>> GetUserFormsAsync(Guid? userId);
        Task<FormDto> GetFormWithAnswersAsync(Guid id);
        Task<FormDto> GetFromWithOneAnswerAsync(Guid formId, Guid correlationToken);

    }
}
