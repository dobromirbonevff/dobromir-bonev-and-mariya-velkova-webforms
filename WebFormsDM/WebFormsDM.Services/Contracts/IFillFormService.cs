﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebFormsDM.Services.DtoEntities;

namespace WebFormsDM.Services.Contracts
{
    public interface IFillFormService
    {
        Task<FillFormDto> CreateFormAnswersAsync(FillFormDto fillFormDto);
        Task<FillFormDto> GetFillFormAsync(Guid? id);
    }
}
