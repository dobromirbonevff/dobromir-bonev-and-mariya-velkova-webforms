﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebFormsDM.Services.DtoEntities;

namespace WebFormsDM.Services.Contracts
{
    public interface IFileAnswersService
    {
        Task<FileAnswersDto> CreateFileAnswerAsync(string path, Guid documentQAID);
        
    }
}
