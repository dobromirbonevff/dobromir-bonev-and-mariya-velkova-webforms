﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.DtoEntities;

namespace WebFormsDM.Services.DtoMapper
{
    public static class OptionQuestionsAnswersExtension
    {
        public static OptionQuestionsAnswersDto GetDto(this OptionQuestionsAnswers optionQuestionsAnswers)
        {
            var optionQuestionsAnswersDto = new OptionQuestionsAnswersDto
            {
                Id = optionQuestionsAnswers.Id,
                //OptionsQuestionDescription = optionQuestionsAnswers.OptionsQuestion.Description,
                OptionsQuestionId = optionQuestionsAnswers.OptionsQuestionId,
                CorrelationToken = optionQuestionsAnswers.CorrelationToken,
                //Options = optionQuestionsAnswers.Options.GetDtos(),
                Answer = optionQuestionsAnswers.Answer


            };

            return optionQuestionsAnswersDto;
        }
        

        public static ICollection<OptionQuestionsAnswersDto> GetDtos
            (this ICollection<OptionQuestionsAnswers> optionQuestionsAnswers)
        {
            return optionQuestionsAnswers.Select(GetDto).ToList();
        }

        public static OptionQuestionsAnswers GetEntitie(this OptionQuestionsAnswersDto optionQuestionsAnswersDto)
        {
            var optionQuestionsAnswers = new OptionQuestionsAnswers
            {
                Id = optionQuestionsAnswersDto.Id,
                //OptionsQuestionDescription = optionQuestionsAnswers.OptionsQuestion.Description,
                OptionsQuestionId = optionQuestionsAnswersDto.OptionsQuestionId,
                CorrelationToken = optionQuestionsAnswersDto.CorrelationToken,
                //Options = optionQuestionsAnswersDto.Options.GetEntities(),
                Answer = optionQuestionsAnswersDto.Answer
            };

            return optionQuestionsAnswers;
        }

        public static ICollection<OptionQuestionsAnswers> GetEntities
            (this ICollection<OptionQuestionsAnswersDto> optionQuestionsAnswersDtos)
        {
            return optionQuestionsAnswersDtos.Select(GetEntitie).ToList();
        }
    }
}
