﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.DtoEntities;

namespace WebFormsDM.Services.DtoMapper
{
    public static class OptionQuestionsExtension
    {
        public static OptionQuestionsDto GetDto(this OptionQuestions question)
        {
            var questionDto = new OptionQuestionsDto
            {
                Id = question.Id,
                Description = question.Description,
                IsRequired = question.IsRequired,
                IsAllowedMultipleAnswers = question.IsAllowedMultipleAnswers,
                OrderNumber = question.OrderNumber,
                FormId = question.FormId,
                Options = question.Options.GetDtos(),
                Answers = question.Answers.GetDtos()
            };

            return questionDto;
        }

        public static ICollection<OptionQuestionsDto> GetDtos(this ICollection<OptionQuestions> questions)
        {
            return questions.Select(GetDto).ToList();
        }

        public static OptionQuestions GetEntitie(this OptionQuestionsDto questionDto)
        {
            var question = new OptionQuestions
            {
                Id = questionDto.Id,
                Description = questionDto.Description,
                IsRequired = questionDto.IsRequired,
                IsAllowedMultipleAnswers = questionDto.IsAllowedMultipleAnswers,
                OrderNumber = questionDto.OrderNumber,
                FormId = questionDto.FormId,
                Options = questionDto.Options.GetEntities(),
                //Answers = questionDto.Answers.GetEntities()
            };

            return question;
        }

        public static ICollection<OptionQuestions> GetEntities(this ICollection<OptionQuestionsDto> questionDtos)
        {
            return questionDtos.Select(GetEntitie).ToList();
        }
    }
}
