﻿using System;
using System.Collections.Generic;
using System.Text;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.DtoEntities;

namespace WebFormsDM.Services.DtoMapper
{
    public static class FillFormDtoExtension
    {
        public static FillFormDto GetFillFormDto(this Form form)
        {
            var allQACount = (form.DocumentQuestions.Count) + (form.OptionsQuestions.Count) + (form.TextQuestions.Count);

            var fillFormDto = new FillFormDto
            {
                Id = form.Id,
                Description = form.Description,
                Title = form.Title,
                TextQuestionAndAnswerDtos = form.TextQuestions.GetTextQuestionAndAnswerDtos(),
                DocumentQuestionAndAnswerDto = form.DocumentQuestions.GetDocumentQuestionAndAnswersDtos(),
                OptionQuestionsAndAnswersDtos = form.OptionsQuestions.GetOptionQuestionsAndAnswersDto(),
                AllQuestionsCount = allQACount

            };
            return fillFormDto;
        }


    }
}
