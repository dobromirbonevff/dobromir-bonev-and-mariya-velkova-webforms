﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.DtoEntities;

namespace WebFormsDM.Services.DtoMapper
{
    public static class UserExtension
    {
        public static UserDto GetDto(this User user)
        {
            var userDto = new UserDto
            {
                Id = user.Id,
                CreatedOn = user.CreatedOn,
                IsDeleted = user.IsDeleted,
                DeletedOn = user.DeletedOn,
                ModifiedOn = user.ModifiedOn,
                Forms = user.Forms.GetDtos()
            };

            return userDto;
        }

        public static ICollection<UserDto> GetDtos(this ICollection<User> users)
        {
            var userDtos = users.Select(GetDto).ToList();

            return userDtos;
        }
    }
}
