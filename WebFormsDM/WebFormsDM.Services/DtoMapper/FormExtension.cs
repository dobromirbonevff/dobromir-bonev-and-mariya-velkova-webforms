﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.DtoEntities;

namespace WebFormsDM.Services.DtoMapper
{
    public static class FormExtension
    {
        //FROM Entity to Dto
        public static FormDto GetDto(this Form form)
        {
            var formDto = new FormDto
            {
                Id = form.Id,
                Title = form.Title,
                Description = form.Description,
                NumberOfFilledForms = form.NumberOfFilledForms,
                UserId = form.UserId,
                CreatedOn = form.CreatedOn,
                DateOfExpiration = form.DateOfExpiration,
                LastModifiedOn = form.LastModifiedOn,
                IsDeleted = form.IsDeleted,
                TextQuestions = form.TextQuestions.GetDtos(),
                OptionQuestions = form.OptionsQuestions.GetDtos(),
                DocumentQuestions = form.DocumentQuestions.GetDtos(),
               

            };

            return formDto;
        }

        public static ICollection<FormDto> GetDtos(this ICollection<Form> forms)
        {
            var formDtos = forms.Select(GetDto).ToList();

            return formDtos;
        }

        //FROM Dto To Entity
        public static Form GetEntity(this FormDto formDto)
        {
            var form = new Form
            {
                Id = formDto.Id,
                Title = formDto.Title,
                Description = formDto.Description,
                NumberOfFilledForms = formDto.NumberOfFilledForms,
                UserId = formDto.UserId,
                CreatedOn = formDto.CreatedOn,
                DateOfExpiration = formDto.DateOfExpiration,
                LastModifiedOn = formDto.LastModifiedOn,
                IsDeleted = formDto.IsDeleted,
                TextQuestions = formDto.TextQuestions.GetEntities(),
                //OptionQuestions = formDto.OptionsQuestions.GetEntities(),
                DocumentQuestions = formDto.DocumentQuestions.GetEntities()
            };

            return form;
        }

        public static ICollection<Form> GetEntities(this ICollection<FormDto> formDtos)
        {
            var form = formDtos.Select(GetEntity).ToList();

            return form;
        }
    }
}




