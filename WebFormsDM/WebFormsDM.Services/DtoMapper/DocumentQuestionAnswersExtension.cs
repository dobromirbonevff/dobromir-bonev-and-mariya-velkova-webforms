﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.DtoEntities;

namespace WebFormsDM.Services.DtoMapper
{
    public static class DocumentQuestionAnswersExtension
    {
        public static DocumentQuestionAnswersDto GetDto(this DocumentQuestionAnswers documentQuestionAnswer)
        {
            var documentQuestionAnswerDto = new DocumentQuestionAnswersDto
            {
                Id = documentQuestionAnswer.Id,
                FileAnswersDto = documentQuestionAnswer.FileAnswers.GetDtos(),
                DocumentQuestionId = documentQuestionAnswer.DocumentQuestionId,
                CorrelationToken = documentQuestionAnswer.CorrelationToken

            };

            return documentQuestionAnswerDto;
        }


        public static ICollection<DocumentQuestionAnswersDto> GetDtos(this ICollection<DocumentQuestionAnswers> documentQuestionAnswers)
        {
            return documentQuestionAnswers.Select(GetDto).ToList();
        }

        public static DocumentQuestionAnswers GetEntitie(this DocumentQuestionAnswersDto documentQuestionAnswerDto)
        {
            var documentQuestionAnswer = new DocumentQuestionAnswers
            {
                Id = documentQuestionAnswerDto.Id,
                FileAnswers = documentQuestionAnswerDto.FileAnswersDto.GetEntities(),
                DocumentQuestionId = documentQuestionAnswerDto.DocumentQuestionId,
                CorrelationToken = documentQuestionAnswerDto.CorrelationToken

            };

            return documentQuestionAnswer;
        }

        public static ICollection<DocumentQuestionAnswers> GetEntities(this ICollection<DocumentQuestionAnswersDto> documentQuestionAnswerDtos)
        {
            return documentQuestionAnswerDtos.Select(GetEntitie).ToList();
        }
    }
}
