﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.DtoEntities;

namespace WebFormsDM.Services.DtoMapper
{
    public static class TextQuestionAnswersExtensions
    {
        public static TextQuestionAnswersDto GetDto(this TextQuestionAnswers textQuestionAnswer)
        {
            var textQuestionAnswerDto = new TextQuestionAnswersDto
            {
                Id = textQuestionAnswer.Id,
                Answer = textQuestionAnswer.Answer,
                TextQuestionId = textQuestionAnswer.TextQuestionId,
              //  TextQuestion = textQuestionAnswer.TextQuestion.GetDto(),
                CorrelationToken = textQuestionAnswer.CorrelationToken

            };

            return textQuestionAnswerDto;
        }


        public static ICollection<TextQuestionAnswersDto> GetDtos(this ICollection<TextQuestionAnswers> textQuestionAnswers)
        {
            return textQuestionAnswers.Select(GetDto).ToList();
        }

        public static TextQuestionAnswers GetEntitie(this TextQuestionAnswersDto textQuestionAnswerDto)
        {
            var textQuestionAnswer = new TextQuestionAnswers
            {
                Id = textQuestionAnswerDto.Id,
                Answer = textQuestionAnswerDto.Answer,
                TextQuestionId = textQuestionAnswerDto.TextQuestionId,
                CorrelationToken = textQuestionAnswerDto.CorrelationToken,
               // TextQuestion = textQuestionAnswerDto.TextQuestion.GetEntity()

            };

            return textQuestionAnswer;
        }

        public static ICollection<TextQuestionAnswers> GetEntities(this ICollection<TextQuestionAnswersDto> textQuestionAnswerDtos)
        {
            return textQuestionAnswerDtos.Select(GetEntitie).ToList();
        }
    }
}
