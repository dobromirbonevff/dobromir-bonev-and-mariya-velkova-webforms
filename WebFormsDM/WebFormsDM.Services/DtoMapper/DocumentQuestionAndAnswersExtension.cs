﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.DtoEntities;

namespace WebFormsDM.Services.DtoMapper
{
    public static class DocumentQuestionAndAnswersExtension
    {
        public static DocumentQuestionAndAnswersDto GetDocumentQuestionAndAnswersDto(this DocumentQuestion documentQuestion)
        {
            var documentQuestionAndAnswerDto = new DocumentQuestionAndAnswersDto
            {
                DocumentQuestionDto = documentQuestion.GetDto(),
                DocumentQuestionAnswersDto = new DocumentQuestionAnswersDto(),

            };

            return documentQuestionAndAnswerDto;
        }

        public static ICollection<DocumentQuestionAndAnswersDto> GetDocumentQuestionAndAnswersDtos(this ICollection<DocumentQuestion> documentQuestions)
        {
            var documentQuestionsAndAnswersDtos = documentQuestions.Select(GetDocumentQuestionAndAnswersDto).ToList();

            return documentQuestionsAndAnswersDtos;
        }
    }
}
