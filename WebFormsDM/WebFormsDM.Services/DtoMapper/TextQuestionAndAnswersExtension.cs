﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.DtoEntities;

namespace WebFormsDM.Services.DtoMapper
{
    public static class TextQuestionAndAnswersExtension
    {
        public static TextQuestionAndAnswersDto GetTextQuestionAndAnswerDto(this TextQuestion textQuestion)
        {
            var textQAndADto = new TextQuestionAndAnswersDto
            {
                TextQuestionDto = textQuestion.GetDto(),
                TextQuestionAnswersDto = new TextQuestionAnswersDto(),
               
            };

            return textQAndADto;
        }

        public static ICollection<TextQuestionAndAnswersDto> GetTextQuestionAndAnswerDtos(this ICollection<TextQuestion> textQuestions)
        {
            return textQuestions.Select(GetTextQuestionAndAnswerDto).ToList();
        }

    }
}
