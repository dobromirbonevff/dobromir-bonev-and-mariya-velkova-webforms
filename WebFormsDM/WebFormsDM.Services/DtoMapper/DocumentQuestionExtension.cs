﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.DtoEntities;

namespace WebFormsDM.Services.DtoMapper
{
    public static class DocumentQuestionExtension
    {
        public static DocumentQuestionDto GetDto(this DocumentQuestion documentQuestion)
        {
            var documentQuestionDto = new DocumentQuestionDto
            {
                Id = documentQuestion.Id,
                Description = documentQuestion.Description,
                FormId = documentQuestion.FormId,
                IsRequired = documentQuestion.IsRequired,
                OrderNumber = documentQuestion.OrderNumber,
                IsDeleted = documentQuestion.IsDeleted,
                FileSize = documentQuestion.FileSize,
                NumberOfFiles = documentQuestion.NumberOfFiles,
                Answers = documentQuestion.Answers.GetDtos()

            };

            return documentQuestionDto;
        }

        public static ICollection<DocumentQuestionDto> GetDtos(this ICollection<DocumentQuestion> documentQuestions)
        {
            return documentQuestions.Select(GetDto).ToList();
        }

        public static DocumentQuestion GetEntitie(this DocumentQuestionDto documentQuestionDto)
        {
            var documentQuestion = new DocumentQuestion
            {
                Id = documentQuestionDto.Id,
                Description = documentQuestionDto.Description,
                FormId = documentQuestionDto.FormId,
                IsRequired = documentQuestionDto.IsRequired,
                OrderNumber = documentQuestionDto.OrderNumber,
                IsDeleted = documentQuestionDto.IsDeleted,
                FileSize = documentQuestionDto.FileSize,
                NumberOfFiles = documentQuestionDto.NumberOfFiles,
                //Answers = documentQuestionDto.Answers.GetEntities()
            };

            return documentQuestion;
        }

        public static ICollection<DocumentQuestion> GetEntities(this ICollection<DocumentQuestionDto> documentQuestionDtos)
        {
            return documentQuestionDtos.Select(GetEntitie).ToList();
        }
    }
}
