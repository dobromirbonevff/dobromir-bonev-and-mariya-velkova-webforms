﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.DtoEntities;

namespace WebFormsDM.Services.DtoMapper
{
    public static class TextQuestionExtension
    {
        public static TextQuestionDto GetDto(this TextQuestion textQuestion)
        {
            var textQuestionDto = new TextQuestionDto
            {
                Id = textQuestion.Id,
                Description = textQuestion.Description,
                IsLongAnswer = textQuestion.IsLongAnswer,
                IsRequired = textQuestion.IsRequired,
                FormId = textQuestion.FormId,
                FormTitle = textQuestion.Form.Title,
                OrderNumber = textQuestion.OrderNumber,
                Answers = textQuestion.Answers.GetDtos()
            };
            return textQuestionDto;
        }

        public static ICollection<TextQuestionDto> GetDtos(this ICollection<TextQuestion> textQuestions)
        {
            var textQuestionDtos = textQuestions.Select(GetDto).ToList();

            return textQuestionDtos;
        }

        public static TextQuestion GetEntity(this TextQuestionDto textQuestionDto)
        {
            var textQuestion = new TextQuestion
            {
                Id = textQuestionDto.Id,
                Description = textQuestionDto.Description,
                IsLongAnswer = textQuestionDto.IsLongAnswer,
                IsRequired = textQuestionDto.IsRequired,
                FormId = textQuestionDto.FormId,
                //Form = textQuestionDto.Form.Get,
                OrderNumber = textQuestionDto.OrderNumber,
                Answers = textQuestionDto.Answers.GetEntities()
            };
            return textQuestion;
        }

        public static ICollection<TextQuestion> GetEntities(this ICollection<TextQuestionDto> textQuestionsDtos)
        {
            var textQuestion = textQuestionsDtos.Select(GetEntity).ToList();

            return textQuestion;
        }
    }
}

