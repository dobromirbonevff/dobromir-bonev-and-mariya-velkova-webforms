﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.DtoEntities;

namespace WebFormsDM.Services.DtoMapper
{
    public static class OptionsQuestionOptionExtension
    {
        public static OptionsQuestionOptionDto GetDto(this OptionsQuestionOption option)
        {
            var optionDto = new OptionsQuestionOptionDto
            {
                Id = option.Id,
                Option = option.Option,
                OptionsQuestionId = option.OptionsQuestionId,
                //Answer = option.Answer.GetDto()
                
            };

            return optionDto;
        }

        public static List<OptionsQuestionOptionDto> GetDtos(this ICollection<OptionsQuestionOption> options)
        {
            return options.Select(GetDto).ToList();
        }

        public static OptionsQuestionOption GetEntitie(this OptionsQuestionOptionDto option)
        {
            var optionEntitie = new OptionsQuestionOption
            {
                Id = option.Id,
                Option = option.Option,
                OptionsQuestionId = option.OptionsQuestionId,
               // Answer = option.Answer.GetEntitie()
            };

            return optionEntitie;
        }

        public static ICollection<OptionsQuestionOption> GetEntities(this ICollection<OptionsQuestionOptionDto> options)
        {
            return options.Select(GetEntitie).ToList();
        }
    }
}
