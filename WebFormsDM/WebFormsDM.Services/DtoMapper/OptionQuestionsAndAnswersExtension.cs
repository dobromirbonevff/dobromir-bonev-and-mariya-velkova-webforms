﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.DtoEntities;

namespace WebFormsDM.Services.DtoMapper
{
    public static class OptionQuestionsAndAnswersExtension
    {
        public static OptionQuestionsAndAnswersDto GetOptionQuestionsAndAnswersDto(this OptionQuestions optionQuestions)
        {
            var optionQuestionsAndAnswersDto = new OptionQuestionsAndAnswersDto
            {
                OptionQuestionsDto = optionQuestions.GetDto(),
                OptionQuestionsAnswersDto = new OptionQuestionsAnswersDto()
            };

            return optionQuestionsAndAnswersDto;
        }

        public static ICollection<OptionQuestionsAndAnswersDto> GetOptionQuestionsAndAnswersDto
            (this ICollection<OptionQuestions> optionQuestions)
        {
            return optionQuestions.Select(GetOptionQuestionsAndAnswersDto).ToList();
        }
    }
}
