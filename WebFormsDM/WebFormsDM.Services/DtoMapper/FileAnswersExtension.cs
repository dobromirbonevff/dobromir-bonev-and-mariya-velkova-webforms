﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.DtoEntities;

namespace WebFormsDM.Services.DtoMapper
{
    public static class FileAnswersExtension
    {
        public static FileAnswersDto GetFileAnswerDto(this FileAnswers fileAnswer)
        {
            var fileAnswerDto = new FileAnswersDto
            {
                Id = fileAnswer.Id,
                FilePath = fileAnswer.FilePath,
                DocumentQAnswerId = fileAnswer.DocumentQAnswerId,
            };

            return fileAnswerDto;
        }

        public static ICollection<FileAnswersDto> GetDtos(this ICollection<FileAnswers> fileAnswers)
        {
            return fileAnswers.Select(GetFileAnswerDto).ToList();
        }

        public static FileAnswers GetEntitie(this FileAnswersDto fileAnswersDto)
        {
            var fileAnswer = new FileAnswers
            {
                Id = fileAnswersDto.Id,
                FilePath = fileAnswersDto.FilePath,
                DocumentQAnswerId = fileAnswersDto.DocumentQAnswerId,

            };

            return fileAnswer;
        }

        public static ICollection<FileAnswers> GetEntities(this ICollection<FileAnswersDto> fileAnswersDtos)
        {
            return fileAnswersDtos.Select(GetEntitie).ToList();
        }

    }
}
