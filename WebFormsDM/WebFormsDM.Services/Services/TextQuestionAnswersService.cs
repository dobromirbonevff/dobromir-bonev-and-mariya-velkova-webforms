﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebFormsDM.Data;
using WebFormsDM.Services.Contracts;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Services.DtoMapper;

namespace WebFormsDM.Services.Services
{
   public  class TextQuestionAnswersService : ITextQuestionAnswersService
    {
        private readonly WebFormsDBContext context;
        public TextQuestionAnswersService(WebFormsDBContext context)
        {
            this.context = context;
        }

        public async Task<TextQuestionAnswersDto> CreateTextQuestionAnswerAsync(TextQuestionAnswersDto textQuestionAnswersDto)
        {
            if (textQuestionAnswersDto == null)
            {
                throw new ArgumentNullException();
            }

            var textQuestionAnswer = textQuestionAnswersDto.GetEntitie();

            
            await this.context.TextQuestionsAnswers.AddAsync(textQuestionAnswer);

            var newTextQuestionAnswerDtos = textQuestionAnswer.GetDto();

            return newTextQuestionAnswerDtos;

        }

    }

}
