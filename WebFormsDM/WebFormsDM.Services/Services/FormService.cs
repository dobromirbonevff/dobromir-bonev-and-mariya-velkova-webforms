﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebFormsDM.Data;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.Contracts;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Services.DtoMapper;

namespace WebFormsDM.Services.Services
{
    public class FormService : IFormService
    {
        private readonly WebFormsDBContext context;
     

        public FormService(WebFormsDBContext context)
        {
            this.context = context;
            
        }

        public async Task<FormDto> CreateFormAsync(FormDto formDto)
        {
            if (formDto == null)
            {
                throw new ArgumentNullException();
            }

            var newForm = new Form
            {
                Title = formDto.Title,
                Description = formDto.Description,
                IsDeleted = false,
                UserId = formDto.UserId,
                TextQuestions = formDto.TextQuestions.GetEntities(),
                OptionsQuestions = formDto.OptionQuestions.GetEntities(),
                DocumentQuestions = formDto.DocumentQuestions.GetEntities(),
            };

            await this.context.Forms.AddAsync(newForm);
            await this.context.SaveChangesAsync();

            var newFormDto = newForm.GetDto();

            return newFormDto;
        }

        public async Task<FormDto> CreateNewVersionOfFormAsync(FormDto formDto)
        {
            if (formDto == null)
            {
                throw new ArgumentNullException();
            }

            var newForm = new Form
            {
                Title = formDto.Title + "(New Version)",
                Description = formDto.Description,
                IsDeleted = false,
                UserId = formDto.UserId,
                TextQuestions = formDto.TextQuestions.GetEntities(),
                OptionsQuestions = formDto.OptionQuestions.GetEntities(),
                DocumentQuestions = formDto.DocumentQuestions.GetEntities(),
            };

            await this.context.Forms.AddAsync(newForm);
            await this.context.SaveChangesAsync();

            var newFormDto = newForm.GetDto();

            return newFormDto;

        }
       
        public async Task<FormDto> GetFormAsync(Guid? id)
        {
            var form = await this.context.Forms
                .Where(f => f.IsDeleted == false)
                .Include(f => f.TextQuestions)           
                .Include(f => f.OptionsQuestions)
                .ThenInclude(oq => oq.Options)
                .Include(f => f.DocumentQuestions)
                .FirstOrDefaultAsync(f => f.Id == id) ?? throw new ArgumentNullException("No form found");

            var formDto = form.GetDto();

            return formDto;
        }

        public async Task<FormDto> GetFormWithAnswersAsync(Guid id)
        {
            var form = await this.context.Forms
                .Where(f => f.IsDeleted == false)
                .FirstOrDefaultAsync(f => f.Id == id);

            form.TextQuestions = await this.context.TextQuestions
                .Where(t => t.FormId == id)
                .Include(t => t.Answers)
                .ToListAsync();

            form.OptionsQuestions = await this.context.OptionsQuestions
                .Where(o => o.FormId == id)
                .Include(o => o.Answers)
                .Include(o => o.Options)
                .ToListAsync();

            form.DocumentQuestions = await this.context.DocumentQuestions
                .Where(d => d.FormId == id)
                .Include(d => d.Answers)
                .ThenInclude(a => a.FileAnswers)
                .ToListAsync();

            var allCorrelationTokens = new List<Guid>();

            foreach (var textQuestion in form.TextQuestions)
            {
                allCorrelationTokens.AddRange(textQuestion.Answers.Select(a => a.CorrelationToken).Distinct());
            }

            foreach (var optionsQuestion in form.OptionsQuestions)
            {
                allCorrelationTokens.AddRange(optionsQuestion.Answers.Select(a => a.CorrelationToken).Distinct());
            }

            foreach (var documentQuestions in form.DocumentQuestions)
            {
                allCorrelationTokens.AddRange(documentQuestions.Answers.Select(a => a.CorrelationToken).Distinct());
            }

            allCorrelationTokens = allCorrelationTokens.Distinct().ToList();
            var formDto = form.GetDto();
            formDto.AllCorrelationTokens = allCorrelationTokens;

            return formDto;
        }

        public async Task<FormDto> GetFromWithOneAnswerAsync(Guid formId, Guid correlationToken)
        {
            var form = await this.context.Forms
                .Where(f => f.IsDeleted == false)
                .FirstOrDefaultAsync(f => f.Id == formId);

            form.TextQuestions = await this.context.TextQuestions
               .Where(t => t.FormId == formId)
               .Include(t => t.Answers)
               .ToListAsync();

            form.OptionsQuestions = await this.context.OptionsQuestions
                .Where(o => o.FormId == formId)
                .Include(o => o.Answers)
                .Include(o => o.Options)
                .ToListAsync();

            form.DocumentQuestions = await this.context.DocumentQuestions
                .Where(d => d.FormId == formId)
                .Include(d => d.Answers)
                .ThenInclude(a => a.FileAnswers)
                .ToListAsync();

            var allCorrelationTokens = new List<Guid>();
            allCorrelationTokens.Add(correlationToken);

            var formDto = form.GetDto();
            formDto.AllCorrelationTokens = allCorrelationTokens;

            return formDto;
        }

        public async Task<ICollection<FormDto>> GetUserFormsAsync(Guid? userId)
        {
            if(userId == null)
            {
                throw new ArgumentNullException();
            }
            
            var forms = await this.context.Forms
               .Where(f => f.IsDeleted == false && f.UserId == userId)
               .ToListAsync() ?? throw new ArgumentNullException("No forms found");

            var formsDto = forms.GetDtos();

            return formsDto;
        }

    }
}
