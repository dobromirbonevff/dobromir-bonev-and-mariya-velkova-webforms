﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebFormsDM.Data;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.Contracts;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Services.DtoMapper;

namespace WebFormsDM.Services.Services
{
    public class FileAnswersService : IFileAnswersService
    {
        private readonly WebFormsDBContext context;

        public FileAnswersService(WebFormsDBContext context)
        {
            this.context = context;
        }
        public async Task<FileAnswersDto> CreateFileAnswerAsync(string path, Guid documentQAID)
        {
            if (path == null)
            {
                throw new ArgumentNullException();
            }
            var fileAnswer = new FileAnswers
            {
                FilePath = path,
                DocumentQAnswerId = documentQAID
            };

            await this.context.FileAnswers.AddAsync(fileAnswer);

            var newfileAnswersDtos = fileAnswer.GetFileAnswerDto();

            return newfileAnswersDtos;

        }


    }
}
