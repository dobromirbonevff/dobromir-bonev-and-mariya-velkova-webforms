﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebFormsDM.Data;
using WebFormsDM.Services.Contracts;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Services.DtoMapper;

namespace WebFormsDM.Services.Services
{
    public class OptionQuestionsAnswersService : IOptionQuestionsAnswersService
    {
        private readonly WebFormsDBContext context;

        public OptionQuestionsAnswersService(WebFormsDBContext context)
        {
            this.context = context;
        }
        public async Task<OptionQuestionsAnswersDto> CreateOptionQuestionAnswerAsync(OptionQuestionsAnswersDto optionQuestionAnswersDto)
        {
            if (optionQuestionAnswersDto == null)
            {
                throw new ArgumentNullException();
            }

            var optionQuestionAnswer = optionQuestionAnswersDto.GetEntitie();


            await this.context.OptionsQuestionsAnswers.AddAsync(optionQuestionAnswer);

            var newoptionQuestionAnswerDtos = optionQuestionAnswer.GetDto();

            return newoptionQuestionAnswerDtos;

        }

    }
}
