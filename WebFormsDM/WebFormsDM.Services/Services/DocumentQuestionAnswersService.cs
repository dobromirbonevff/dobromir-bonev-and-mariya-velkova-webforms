﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebFormsDM.Data;
using WebFormsDM.Services.Contracts;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Services.DtoMapper;

namespace WebFormsDM.Services.Services
{
    public class DocumentQuestionAnswersService : IDocumentQuestionAnswersService
    {
        private readonly WebFormsDBContext context;
        public DocumentQuestionAnswersService(WebFormsDBContext context)
        {
            this.context = context;
        }

        public async Task<DocumentQuestionAnswersDto> CreateDocumentQuestionAnswerAsync(DocumentQuestionAnswersDto documentQuestionAnswerDto)
        {
            if (documentQuestionAnswerDto == null)
            {
                throw new ArgumentNullException();
            }

            var documentQuestionAnswer = documentQuestionAnswerDto.GetEntitie();

            await this.context.DocumentQuestionAnswers.AddAsync(documentQuestionAnswer);           

            var newDocumentQuestionAnswerDto = documentQuestionAnswer.GetDto();

            return newDocumentQuestionAnswerDto;

        }

    }
}
