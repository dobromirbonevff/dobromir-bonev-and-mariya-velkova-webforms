﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebFormsDM.Data;
using WebFormsDM.Data.Entities;
using WebFormsDM.Services.Contracts;
using WebFormsDM.Services.DtoEntities;
using WebFormsDM.Services.DtoMapper;

namespace WebFormsDM.Services.Services
{
    public class FillFormService : IFillFormService
    {
        private readonly WebFormsDBContext context;
        private readonly IFileAnswersService fileAnswerService;
        private readonly IDocumentQuestionAnswersService documentQuestionAnswersService;
        private readonly ITextQuestionAnswersService textQuestionAnswersService;
        private readonly IOptionQuestionsAnswersService optionQuestionsAnswersService;


        public FillFormService(WebFormsDBContext context,
             IFileAnswersService fileAnswerService,
             IDocumentQuestionAnswersService documentQuestionAnswersService,
             ITextQuestionAnswersService textQuestionAnswersService,
             IOptionQuestionsAnswersService optionQuestionsAnswersService)
        {
            this.context = context;
            this.fileAnswerService = fileAnswerService;
            this.documentQuestionAnswersService = documentQuestionAnswersService;
            this.textQuestionAnswersService = textQuestionAnswersService;
            this.optionQuestionsAnswersService = optionQuestionsAnswersService;

        }


        public async Task<FillFormDto> CreateFormAnswersAsync(FillFormDto fillFormDto)
        {
            if (fillFormDto == null)
            {
                throw new ArgumentNullException();
            }
            Guid currentAnswerCToken = Guid.NewGuid();

            //Create Document Question Answers

            foreach (var item in fillFormDto.DocumentQuestionAndAnswerDto)
            {
                if (item.DocumentQuestionAnswersDto.AnsewrPaths.Count != 0)
                {
                    var documentQAnswerDto = item.DocumentQuestionAnswersDto;
                    documentQAnswerDto.CorrelationToken = currentAnswerCToken;

                    var currentAnswer = await this.documentQuestionAnswersService.CreateDocumentQuestionAnswerAsync(documentQAnswerDto);
                    var currentAnswerId = currentAnswer.Id;

                    foreach (var path in item.DocumentQuestionAnswersDto.AnsewrPaths)
                    {
                        await this.fileAnswerService.CreateFileAnswerAsync(path, currentAnswerId);
                    }
                }
             
            }

            //Create Text Question Answers

            foreach (var item in fillFormDto.TextQuestionAndAnswerDtos)
            {
                if (item.TextQuestionAnswersDto.Answer != null)
                {
                    var textQAnswerDto = item.TextQuestionAnswersDto;
                    textQAnswerDto.CorrelationToken = currentAnswerCToken;

                    await this.textQuestionAnswersService.CreateTextQuestionAnswerAsync(textQAnswerDto);
                }
                
            }

            //Create Option Question Answers
            foreach (var item in fillFormDto.OptionQuestionsAndAnswersDtos)
            {
                if (item.OptionQuestionsAnswersDto.Answer != "NA")
                {
                    var optionQAnswerDto = item.OptionQuestionsAnswersDto;
                    optionQAnswerDto.CorrelationToken = currentAnswerCToken;

                    await this.optionQuestionsAnswersService.CreateOptionQuestionAnswerAsync(optionQAnswerDto);
                }
               
            }

            var form = await this.context.Forms.Where(f => f.Id == fillFormDto.Id).FirstOrDefaultAsync();
            form.NumberOfFilledForms++;
            
            this.context.Update(form);

            await this.context.SaveChangesAsync();


            return fillFormDto;
        }
        public async Task<FillFormDto> GetFillFormAsync(Guid? id)
        {
            var form = await this.context.Forms
                .Where(f => f.IsDeleted == false)
                .Include(f => f.TextQuestions)
                .Include(f => f.OptionsQuestions)
                .ThenInclude(oq => oq.Options)
                .Include(f => f.DocumentQuestions)
                .FirstOrDefaultAsync(f => f.Id == id) ?? throw new ArgumentNullException("No form found");

            var fillFormDto = form.GetFillFormDto();
            return fillFormDto;

        }
    }

}

