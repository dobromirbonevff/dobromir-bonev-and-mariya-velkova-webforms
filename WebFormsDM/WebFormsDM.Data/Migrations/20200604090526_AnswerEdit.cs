﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebFormsDM.Data.Migrations
{
    public partial class AnswerEdit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Answer",
                table: "OptionsQuestionsAnswers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Answer",
                table: "OptionsQuestionsAnswers");
        }
    }
}
