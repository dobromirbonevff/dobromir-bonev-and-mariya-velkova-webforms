﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebFormsDM.Data.Migrations
{
    public partial class OptionQuestionConfigurations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "OptionQuestionsAnswersId",
                table: "OptionsQuestionOptions",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_OptionsQuestionOptions_OptionQuestionsAnswersId",
                table: "OptionsQuestionOptions",
                column: "OptionQuestionsAnswersId");

            migrationBuilder.AddForeignKey(
                name: "FK_OptionsQuestionOptions_OptionsQuestionsAnswers_OptionQuestionsAnswersId",
                table: "OptionsQuestionOptions",
                column: "OptionQuestionsAnswersId",
                principalTable: "OptionsQuestionsAnswers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OptionsQuestionOptions_OptionsQuestionsAnswers_OptionQuestionsAnswersId",
                table: "OptionsQuestionOptions");

            migrationBuilder.DropIndex(
                name: "IX_OptionsQuestionOptions_OptionQuestionsAnswersId",
                table: "OptionsQuestionOptions");

            migrationBuilder.DropColumn(
                name: "OptionQuestionsAnswersId",
                table: "OptionsQuestionOptions");
        }
    }
}
