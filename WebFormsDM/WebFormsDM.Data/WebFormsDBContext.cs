﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using WebFormsDM.Data.Configurations;
using WebFormsDM.Data.Entities;

namespace WebFormsDM.Data
{
    public class WebFormsDBContext : IdentityDbContext<User, Role, Guid>
    {
        public WebFormsDBContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Form> Forms { get; set; }
        public DbSet<TextQuestion> TextQuestions { get; set; }
        public DbSet<OptionQuestions> OptionsQuestions { get; set; }
        public DbSet<OptionsQuestionOption> OptionsQuestionOptions { get; set; }
        public DbSet<TextQuestionAnswers> TextQuestionsAnswers { get; set; }
        public DbSet<OptionQuestionsAnswers> OptionsQuestionsAnswers { get; set; }
        public DbSet<DocumentQuestion> DocumentQuestions { get; set; }
        public DbSet<DocumentQuestionAnswers> DocumentQuestionAnswers { get; set; }
        public DbSet<FileAnswers> FileAnswers { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new UserConfiguration());
            builder.ApplyConfiguration(new RoleConfiguration());
            builder.ApplyConfiguration(new FormConfiguration());
            builder.ApplyConfiguration(new TextQuestionConfiguration());
            builder.ApplyConfiguration(new TextQuestionAnswersConfiguration());
            builder.ApplyConfiguration(new OptionQuestionsConfiguration());
            builder.ApplyConfiguration(new OptionsQuestionOptionConfiguration());
            builder.ApplyConfiguration(new OptionsQuestionAnswersConfiguration());
            builder.ApplyConfiguration(new DocumentQuestionConfiguration());
            builder.ApplyConfiguration(new DocumentQuestionAnswersConfiguration());
            builder.ApplyConfiguration(new FileAnswersConfiguration());

            base.OnModelCreating(builder);
        }
    }
}
