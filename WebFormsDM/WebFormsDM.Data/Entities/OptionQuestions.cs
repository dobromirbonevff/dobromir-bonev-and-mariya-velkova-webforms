﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFormsDM.Data.Entities
{
    public class OptionQuestions
    {
        public OptionQuestions()
        {
            this.Options = new List<OptionsQuestionOption>();
            this.Answers = new List<OptionQuestionsAnswers>();
        }
        public Guid Id { get; set; }
        public string Description { get; set; }
        public bool IsRequired { get; set; }
        public Guid FormId { get; set; }
        public Form Form { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsAllowedMultipleAnswers { get; set; }
        public int OrderNumber { get; set; }
        public ICollection<OptionsQuestionOption> Options { get; set; }
        public ICollection<OptionQuestionsAnswers> Answers { get; set; }

        

    }
}
