﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace WebFormsDM.Data.Entities
{
    public class User : IdentityUser<Guid>
    {
        public User()
        {
            this.Forms = new List<Form>();
        }
        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
        public bool IsDeleted { get; set; }
        public DateTime? DeletedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public ICollection<Form> Forms { get; set; }
    }
}
