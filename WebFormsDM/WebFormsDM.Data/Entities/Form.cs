﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace WebFormsDM.Data.Entities
{
    public class Form
    {
        public Form()
        {
            this.TextQuestions = new List<TextQuestion>();
            this.OptionsQuestions = new List<OptionQuestions>();
            this.DocumentQuestions = new List<DocumentQuestion>();
        }  
        public Guid Id { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
        public DateTime DateOfExpiration { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        public bool IsDeleted { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int NumberOfFilledForms { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
        public ICollection<TextQuestion> TextQuestions { get; set; }
        public ICollection<OptionQuestions> OptionsQuestions { get; set; }
        public ICollection<DocumentQuestion> DocumentQuestions { get; set; }
    }
}
