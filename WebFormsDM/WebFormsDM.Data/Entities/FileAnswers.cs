﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFormsDM.Data.Entities
{
    public class FileAnswers
    {
        public Guid Id { get; set; }
        public string FilePath { get; set; }
        public Guid DocumentQAnswerId { get; set; }
        public DocumentQuestionAnswers DocumentQuestionAswer { get; set; }
    }
}
