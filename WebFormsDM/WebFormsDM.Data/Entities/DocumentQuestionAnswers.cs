﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFormsDM.Data.Entities
{
    public class DocumentQuestionAnswers
    {
        public DocumentQuestionAnswers()
        {
            this.FileAnswers = new List<FileAnswers>();
        }
        public Guid Id { get; set; }
          
        public Guid DocumentQuestionId { get; set; }
        public DocumentQuestion DocumentQuestion { get; set; }
        public Guid CorrelationToken { get; set; }
        public ICollection<FileAnswers> FileAnswers { get; set; }
    }
}
