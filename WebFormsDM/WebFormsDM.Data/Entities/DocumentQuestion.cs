﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFormsDM.Data.Entities
{
    public class DocumentQuestion
    {
        public DocumentQuestion()
        {
            this.Answers = new List<DocumentQuestionAnswers>();
        }
        public Guid Id { get; set; }
        public string Description { get; set; }
        public bool IsRequired { get; set; }
        public int FileSize { get; set; }
        public int NumberOfFiles { get; set;}
        public Guid FormId { get; set; }
        public Form Form { get; set; }
        public int OrderNumber { get; set; }
        public bool IsDeleted { get; set; }       
        public ICollection<DocumentQuestionAnswers> Answers { get; set; }
    }
}
