﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFormsDM.Data.Entities
{
    public class TextQuestion
    {
        public TextQuestion()
        {
            this.Answers = new List<TextQuestionAnswers>();
        }

        public Guid Id { get; set; }
        public string Description { get; set; }
        public bool IsLongAnswer { get; set; }
        public bool IsRequired { get; set; }
        public Guid FormId { get; set; }
        public Form Form { get; set; }
        public int OrderNumber { get; set; }
        public bool IsDeleted { get; set; }
        public ICollection<TextQuestionAnswers> Answers { get; set; }
    }
}
