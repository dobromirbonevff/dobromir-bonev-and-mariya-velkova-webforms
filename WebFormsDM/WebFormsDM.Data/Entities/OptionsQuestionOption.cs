﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFormsDM.Data.Entities
{
    public class OptionsQuestionOption
    {
        public Guid Id { get; set; }
        public string Option { get; set; }
        public Guid OptionsQuestionId { get; set; }
        public OptionQuestions OptionsQuestion { get; set; }
        public bool IsDeleted { get; set; }
        public Guid? OptionQuestionsAnswersId { get; set; }
        public OptionQuestionsAnswers Answer { get; set; }
    }
}
