﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFormsDM.Data.Entities
{
    public class OptionQuestionsAnswers
    {
        public OptionQuestionsAnswers()
        {
            this.Options = new List<OptionsQuestionOption>();
        }
        public Guid Id { get; set; }
        public Guid OptionsQuestionId { get; set; }
        public OptionQuestions OptionsQuestion { get; set; }
        public Guid CorrelationToken { get; set; }
        public ICollection<OptionsQuestionOption> Options { get; set; }
        public string Answer { get; set; }
        
    }
}
