﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebFormsDM.Data.Entities
{
    public class TextQuestionAnswers
    {
        public Guid Id { get; set; }
        public string Answer { get; set; }
        public Guid TextQuestionId { get; set; }
        public TextQuestion TextQuestion { get; set; }
        public Guid CorrelationToken { get; set; }
    }
}
