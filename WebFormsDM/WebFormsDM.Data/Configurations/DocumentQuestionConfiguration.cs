﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using WebFormsDM.Data.Entities;

namespace WebFormsDM.Data.Configurations
{
    public class DocumentQuestionConfiguration : IEntityTypeConfiguration<DocumentQuestion>
    {
        public void Configure(EntityTypeBuilder<DocumentQuestion> builder)
        {
            builder
                .HasKey(d => d.Id);

            builder
                .Property(d => d.Description)
                .IsRequired();

            builder
                .HasOne(d => d.Form)
                .WithMany(f => f.DocumentQuestions)
                .HasForeignKey(d => d.FormId);
        }
    }
}
