﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using WebFormsDM.Data.Entities;

namespace WebFormsDM.Data.Configurations
{
    public class DocumentQuestionAnswersConfiguration : IEntityTypeConfiguration<DocumentQuestionAnswers>
    {
        public void Configure(EntityTypeBuilder<DocumentQuestionAnswers> builder)
        {
            builder
               .HasKey(a => a.Id);

            builder
                .HasOne(a => a.DocumentQuestion)
                .WithMany(d => d.Answers)
                .HasForeignKey(a => a.DocumentQuestionId);
        }
    }
}
