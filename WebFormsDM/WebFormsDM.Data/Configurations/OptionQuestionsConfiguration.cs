﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using WebFormsDM.Data.Entities;

namespace WebFormsDM.Data.Configurations
{
    public class OptionQuestionsConfiguration : IEntityTypeConfiguration<OptionQuestions>
    {
        public void Configure(EntityTypeBuilder<OptionQuestions> builder)
        {
            builder
                .HasKey(o => o.Id);

            builder
                .Property(o => o.Description)
                .IsRequired();

            builder
                .HasOne(o => o.Form)
                .WithMany(f => f.OptionsQuestions)
                .HasForeignKey(o => o.FormId);
        }
    }
}
