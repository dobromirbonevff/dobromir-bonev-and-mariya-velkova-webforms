﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using WebFormsDM.Data.Entities;

namespace WebFormsDM.Data.Configurations
{
    public class TextQuestionAnswersConfiguration : IEntityTypeConfiguration<TextQuestionAnswers>
    {
        public void Configure(EntityTypeBuilder<TextQuestionAnswers> builder)
        {
            builder
                .HasKey(a => a.Id);

            builder
                .Property(a => a.Answer)
                .IsRequired();

            builder
                .HasOne(a => a.TextQuestion)
                .WithMany(t => t.Answers)
                .HasForeignKey(a => a.TextQuestionId);
        }
    }
}
