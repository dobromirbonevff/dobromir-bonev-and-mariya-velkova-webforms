﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using WebFormsDM.Data.Entities;

namespace WebFormsDM.Data.Configurations
{
    public class OptionsQuestionAnswersConfiguration : IEntityTypeConfiguration<OptionQuestionsAnswers>
    {
        public void Configure(EntityTypeBuilder<OptionQuestionsAnswers> builder)
        {
            builder
                .HasKey(a => a.Id);
            builder
                .HasOne(a => a.OptionsQuestion)
                .WithMany(o => o.Answers)
                .HasForeignKey(a => a.OptionsQuestionId);

        }
    }
}
