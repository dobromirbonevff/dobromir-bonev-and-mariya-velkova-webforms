﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using WebFormsDM.Data.Entities;

namespace WebFormsDM.Data.Configurations
{
    public class FileAnswersConfiguration : IEntityTypeConfiguration<FileAnswers>
    {
        public void Configure(EntityTypeBuilder<FileAnswers> builder)
        {
            builder
                .HasKey(fa => fa.Id);

            builder
                .Property(fa => fa.FilePath)
                .IsRequired();

            builder
                .HasOne(fa => fa.DocumentQuestionAswer)
                .WithMany(dqa => dqa.FileAnswers)
                .HasForeignKey(fa => fa.DocumentQAnswerId);
        }

    }
}
