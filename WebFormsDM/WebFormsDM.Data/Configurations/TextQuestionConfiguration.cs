﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using WebFormsDM.Data.Entities;

namespace WebFormsDM.Data.Configurations
{
    public class TextQuestionConfiguration : IEntityTypeConfiguration<TextQuestion>
    {
        public void Configure(EntityTypeBuilder<TextQuestion> builder)
        {
            builder
                .HasKey(t => t.Id);

            builder
                .Property(t => t.Description)
                .IsRequired();

            builder
                .HasOne(t => t.Form)
                .WithMany(f => f.TextQuestions)
                .HasForeignKey(t => t.FormId);
        }
    }
}
