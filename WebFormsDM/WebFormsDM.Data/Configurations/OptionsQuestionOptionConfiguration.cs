﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using WebFormsDM.Data.Entities;

namespace WebFormsDM.Data.Configurations
{
    public class OptionsQuestionOptionConfiguration : IEntityTypeConfiguration<OptionsQuestionOption>
    {
        public void Configure(EntityTypeBuilder<OptionsQuestionOption> builder)
        {
            builder
                .HasKey(o => o.Id);

            builder
                .Property(o => o.Option);

            builder
                .HasOne(o => o.OptionsQuestion)
                .WithMany(o => o.Options)
                .HasForeignKey(o => o.OptionsQuestionId);

            builder
               .HasOne(oq => oq.Answer)
               .WithMany(o => o.Options)
               .HasForeignKey(oq => oq.OptionQuestionsAnswersId);
        }
    }
}
