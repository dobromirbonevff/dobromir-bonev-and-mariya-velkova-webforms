﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using WebFormsDM.Data.Entities;

namespace WebFormsDM.Data.Seeder
{
    public static class ModelBuilderExtension
    {
        public static void Seeder(this ModelBuilder builder)
        {
            //Role
            builder.Entity<Role>().HasData(
                new Role { Id = Guid.Parse("CDDE12B9-E61A-4748-A239-C7331B4FB6A8"), Name = "Admin", NormalizedName = "ADMIN" },
                new Role { Id = Guid.Parse("43A08CC4-76AE-46D8-9E2C-CDE7B0479146"), Name = "Member", NormalizedName = "MEMBER" }
            );

            //Admin Account
            var hasher = new PasswordHasher<User>();

            User adminUser = new User
            {
                Id = Guid.Parse("AEA4F481-DF4B-4272-9D12-022293D98E48"),
                UserName = "admin@admin.com",
                NormalizedUserName = "ADMIN@ADMIN.COM",
                Email = "admin@admin.com",
                NormalizedEmail = "ADMIN@ADMIN.COM",
                CreatedOn = DateTime.UtcNow,
                LockoutEnabled = true,
                SecurityStamp = "DC6E275DD1E24957A7781D42BB68293B"
            };

            adminUser.PasswordHash = hasher.HashPassword(adminUser, "admin1");

            builder.Entity<User>().HasData(adminUser);

            builder.Entity<IdentityUserRole<Guid>>().HasData(
                new IdentityUserRole<Guid>
                {
                    RoleId = Guid.Parse("CDDE12B9-E61A-4748-A239-C7331B4FB6A8"),
                    UserId = adminUser.Id
                });
        }
    }
}
