# Dobromir Bonev and Mariya Velkova - WebForms 
Web Forms is a web application, which let its registered users create, edit and send via email their surveys. When the survey has been received, it could be filled out by the people which is sent to and its user creator could see its different answers.
The application have:
• public part (accessible without authentication)
• private part (available for registered users) 
The public part consists of the login page, register page and answer form page.
Private part in the web application is accessible after successful login. Any user is able to register and create account.
After successfull login every user see "Create Form" and "Forms" button.
"Forms" button oppens forms list page, where the user sees all of his forms as cards, which consist:
• Name of the form
• When was the form created
• Current answers count
• Share button - opens modal that let user to send link to public answer form page.
• Preview button - opens oublic answer form page.

"Create Form" button oppens create form page, where user can create his oun survey form. 
There 3 tipes of question that user can add in the form:
• Text question -  allows users to answer with free text. There are two settings of this question (implemented as checkboxes) - short answer and long answer.
• Options question - allows user to define custom options to be selected when answered either like radiobutton list or checkbox list, depending of that answer allows multiple answers or not.
By default there are two option added for newly created questions. There is add button for adding new option – newly added option is on the bottom. Each option has a input for name.
• Document qestion -  allows user to attach document as answer. There are two settings for this question - file number limit: dropdown menu  which allows to choose 1-10 and file size limit: dropdown: 1MB, 10MB, 100MB. 
Each question has optio for required answer or non required. 
On the bottom of this page has submit button. After submit the user see the form list page, where can see created form as card with "Share" and "Preview" button.
"Share" button opens modal that allow user to send the form via e-mail.
When opens the mail, receiver can see link to the public answer form page, where to submit his answers. After submit, if answers are accepted, the respondent see page with success message and button "Answer again".
When press "Preview" button the user see the public answer form page and "Edit" and "Answers" buttons on top.
"Edit" button shows page, where user can see the current form in edit mode, so that he can change it. Edit page allows the user to add or delete options, to add, change and delete questions. After he press "Save" button on bottom, new version of form is created. In that way user can see the answers of each version. If there's no ansers yet the user see page that there's no answers yet and back button.
In case, somebody make mistake with page url there is 404(Page Not Found) middleware.

Enjoy our application!


